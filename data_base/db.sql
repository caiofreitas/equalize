
CREATE TABLE t_country (
  id INT NOT NULL AUTO_INCREMENT ,
  name VARCHAR(45) NOT NULL ,
  acronym VARCHAR(45) NULL ,
  PRIMARY KEY (id))
ENGINE = InnoDB;


CREATE TABLE t_state (
  id INT NOT NULL AUTO_INCREMENT ,
  name VARCHAR(45) NOT NULL ,
  acronym VARCHAR(45) NULL ,
  country_id INT NOT NULL ,
  PRIMARY KEY (id) ,
  FOREIGN KEY (country_id)
    REFERENCES t_country (id))
ENGINE = InnoDB;


CREATE TABLE t_city (
  id INT NOT NULL AUTO_INCREMENT ,
  name VARCHAR(45) NOT NULL ,
  state_id INT NOT NULL ,
  PRIMARY KEY (id) ,
   FOREIGN KEY (state_id)
    REFERENCES t_state (id))
ENGINE = InnoDB;


CREATE TABLE t_band (
  id INT NOT NULL AUTO_INCREMENT ,
  name VARCHAR(45) NULL ,
  members VARCHAR(300) NULL ,
  producer VARCHAR(45) NULL ,
  description VARCHAR(300) NULL ,
  PRIMARY KEY (id))
ENGINE = InnoDB;


CREATE TABLE t_address (
  id INT NOT NULL AUTO_INCREMENT ,
  cep VARCHAR(45) NULL ,
  number VARCHAR(10) NULL ,
  street VARCHAR(45) NULL ,
  complement VARCHAR(45) NULL ,
  neighborhood VARCHAR(45) NULL ,
  city_id INT NOT NULL ,
  PRIMARY KEY (id) ,
  FOREIGN KEY (city_id)
    REFERENCES t_city (id))
ENGINE = InnoDB;


CREATE TABLE t_place (
  id INT NOT NULL AUTO_INCREMENT ,
  name VARCHAR(45) NULL ,
  description VARCHAR(300) NULL ,
  address_id INT NOT NULL ,
  PRIMARY KEY (id) ,
  FOREIGN KEY (address_id)
    REFERENCES t_address (id))
ENGINE = InnoDB;


CREATE TABLE t_date (
  id INT NOT NULL AUTO_INCREMENT ,
  date TIMESTAMP NULL ,
  PRIMARY KEY (id))
ENGINE = InnoDB;


CREATE TABLE t_attraction (
  id INT NOT NULL AUTO_INCREMENT ,
  name VARCHAR(45) NULL ,
  ageRange INT NULL ,
  place_id INT NULL ,
  date_id INT NOT NULL ,
  classification_id INT NULL ,
  PRIMARY KEY (id) ,
  FOREIGN KEY (place_id)
    REFERENCES t_place (id),   
  FOREIGN KEY (date_id)
    REFERENCES t_date (id)
    
   )
ENGINE = InnoDB;


CREATE TABLE t_movie (
  id INT NOT NULL AUTO_INCREMENT ,
  name VARCHAR(45) NULL ,
  cast VARCHAR(300) NULL ,
  director VARCHAR(45) NULL ,
  synopsis VARCHAR(300) NULL ,
  PRIMARY KEY (id))
ENGINE = InnoDB;


CREATE TABLE t_play (
  id INT NOT NULL AUTO_INCREMENT ,
  name VARCHAR(45) NULL ,
  cast VARCHAR(300) NULL ,
  director VARCHAR(45) NULL ,
  synopsis VARCHAR(300) NULL ,
  PRIMARY KEY (id))
ENGINE = InnoDB;


CREATE TABLE t_event (
  id INT NOT NULL AUTO_INCREMENT ,
  name VARCHAR(45) NULL ,
  producer VARCHAR(45) NULL ,
  metadata VARCHAR(300) NULL ,
  description VARCHAR(300) NULL ,
  PRIMARY KEY (id))
ENGINE = InnoDB;


CREATE TABLE t_user (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(45) NULL ,
  nickName VARCHAR(45) NULL ,
  email VARCHAR(45) NULL ,
  password VARCHAR(200) null,
  groups VARCHAR(45) NULL ,
  address_id INT NULL,
  PRIMARY KEY (id) ,
  FOREIGN KEY (address_id)
  	REFERENCES t_address (id),
  UNIQUE (nickName)
    ) 
ENGINE = InnoDB;

CREATE TABLE t_likes (
  id INT NOT NULL AUTO_INCREMENT,
  user_id INT NOT NULL ,
  metaData VARCHAR(100) NULL ,
  PRIMARY KEY (id) ,
  FOREIGN KEY (user_id)
    REFERENCES t_user (id))
ENGINE = InnoDB;


CREATE TABLE t_image (
  id INT NOT NULL AUTO_INCREMENT ,
  bytes MEDIUMBLOB NULL ,
  thumbnail MEDIUMBLOB NULL,
  description VARCHAR(45) NULL ,
  name VARCHAR(45) NULL ,
  wight INT NULL ,
  height INT NULL ,
  band_id INT NULL ,
  movie_id INT NULL ,
  play_id INT NULL ,
  attraction_id INT NULL ,
  event_id INT NULL ,
  place_id INT NULL ,
  user_id INT NULL ,
  PRIMARY KEY (id) ,
  FOREIGN KEY (band_id)
    REFERENCES t_band (id),
  FOREIGN KEY (movie_id)
    REFERENCES t_movie (id),
  FOREIGN KEY (play_id)
    REFERENCES t_play (id),
  FOREIGN KEY (attraction_id)
    REFERENCES t_attraction (id),
  FOREIGN KEY (event_id)
    REFERENCES t_event (id),
  FOREIGN KEY (place_id)
    REFERENCES t_place (id),
  FOREIGN KEY (user_id)
    REFERENCES t_user (id))
ENGINE = InnoDB;


CREATE TABLE r_attraction_has_band (
  attraction_id INT NOT NULL ,
  band_id INT NOT NULL ,
  PRIMARY KEY (attraction_id, band_id) ,
  FOREIGN KEY (attraction_id)
    REFERENCES t_attraction (id),
  FOREIGN KEY (band_id)
    REFERENCES t_band(id))
ENGINE = InnoDB;


CREATE TABLE r_attraction_has_movie (
  attraction_id INT NOT NULL ,
  movie_id INT NOT NULL ,
  PRIMARY KEY (attraction_id, movie_id) ,
  FOREIGN KEY (attraction_id)
    REFERENCES t_attraction (id),
  FOREIGN KEY (movie_id)
    REFERENCES t_movie (id))
ENGINE = InnoDB;


CREATE TABLE r_attraction_has_play (
  attraction_id INT NOT NULL ,
  play_id INT NOT NULL ,
  PRIMARY KEY (attraction_id, play_id) ,
  FOREIGN KEY (attraction_id)
    REFERENCES t_attraction (id),
  FOREIGN KEY (play_id)
    REFERENCES t_play (id))
ENGINE = InnoDB;


CREATE TABLE r_attraction_has_event (
  attraction_id INT NOT NULL ,
  event_id INT NOT NULL ,
  PRIMARY KEY (attraction_id, event_id) ,
  FOREIGN KEY (attraction_id)
    REFERENCES t_attraction (id),
  FOREIGN KEY (event_id)
    REFERENCES t_event (id))
ENGINE = InnoDB;


CREATE TABLE t_phone (
  id INT NOT NULL AUTO_INCREMENT,
  code VARCHAR(45) NULL ,
  number VARCHAR(45) NULL ,
  type VARCHAR(45) NULL ,
  attraction_id INT NULL ,
  place_id INT NULL ,
  user_id INT NULL ,
  PRIMARY KEY (id) ,
  FOREIGN KEY (attraction_id)
    REFERENCES t_attraction (id),
  FOREIGN KEY (place_id)
    REFERENCES t_place (id),
  FOREIGN KEY (user_id)
    REFERENCES t_user (id))
ENGINE = InnoDB;


CREATE TABLE t_classification (
  id INT NOT NULL AUTO_INCREMENT,
  attraction_id INT NOT NULL ,
  PRIMARY KEY (id) ,
  FOREIGN KEY (attraction_id)
    REFERENCES t_attraction (id))
ENGINE = InnoDB;


CREATE TABLE r_user_has_classification (
  user_id INT NOT NULL ,
  classification_id INT NOT NULL ,
  PRIMARY KEY (user_id, classification_id) ,
  FOREIGN KEY (user_id)
    REFERENCES t_user (id),
  FOREIGN KEY (classification_id)
    REFERENCES t_classification (id))
ENGINE = InnoDB;



