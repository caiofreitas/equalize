package br.com;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.media.jai.Interpolation;
import javax.media.jai.JAI;
import javax.media.jai.PlanarImage;
import javax.media.jai.RenderedOp;
import javax.media.jai.operator.ScaleDescriptor;

public class Sha1Test {
	
	public static void main(String[] args) {
		
	}
	
	public void reizeImage() {
		
		try {
			
			PlanarImage image = JAI.create("fileload", "/home/caio/Pictures/arcify-2560x1600.jpg");
			int destWidth = 100;
			float xScale = (float) destWidth / image.getWidth();
			float yScale = 0f;
			yScale = xScale;
			RenderedOp renderedOp = ScaleDescriptor.create(image, new Float(xScale), new Float(yScale),
					new Float(0.0f), new Float(0.0f), Interpolation.getInstance(Interpolation.INTERP_BICUBIC), null);
			writeJPEG(renderedOp.getAsBufferedImage(), "/home/caio/Desktop/output.jpg");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void writeJPEG(BufferedImage input, String name) throws IOException {
		Iterator iter = ImageIO.getImageWritersByFormatName("JPG");
		if (iter.hasNext()) {
			ImageWriter writer = (ImageWriter) iter.next();
			ImageWriteParam iwp = writer.getDefaultWriteParam();
			iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			iwp.setCompressionQuality(0.95f);
			File outFile = new File(name);
			FileImageOutputStream output = new FileImageOutputStream(outFile);
			writer.setOutput(output);
			IIOImage image = new IIOImage(input, null, null);
			writer.write(null, image, iwp);
			output.close();
		}
	}
	
}
