<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="../theme/head.jspf" %>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>${pageName}</title>
	<!-- File uploader -->
	<script type='text/javascript' src='<c:url value="/resources/js/fileuploader.js"/>'></script>
	<!-- uploadeditgallery -->
	<script type='text/javascript' src='<c:url value="/resources/js/uploadeditgallery.js"/>'></script>
	<link rel='stylesheet' href='<c:url value="/resources/css/uploadeditgallery.css"/>' type='text/css' media='screen' />
	<script type="text/javascript">
		$(function() { 
			$( "#state" ).autocomplete({
				source: function( request, response ) {
					$.ajax({
						url: APP_CONTEXT + "state.json",
						dataType: "json",
						data: {
							term: request.term
						},
						success: function( data ) {
							response( $.map( data, function( item ) {
								return {
									label: item.name + " - " + item.acronym ,
									value: item.name ,
									id : item.id
								}
							}));
						}
					});
				},
				select: function( event, ui ) {
					$("#mainForm input[name=state]").val(ui.item.id);
				},	
				minLength: 2
			});
			
			$( "#city" ).autocomplete({
				source: function( request, response ) {
					$.ajax({
						url: APP_CONTEXT + "city.json",
						dataType: "json",
						data: {
							term: request.term,
							stateId: $("#mainForm input[name=state]").val()
						},
						success: function( data ) {
							response( $.map( data, function( item ) {
								return {
									label: item.name ,
									value: item.name ,
									id : item.id
								}
							}));
						}
					});
				},
				select: function( event, ui ) {
					$("#mainForm input[name=city]").val(ui.item.id);
				},	
				minLength: 2
			});
			$("#upEditGallery").upEditGallery({'APP_CONTEXT' : APP_CONTEXT });
		});
	</script>
</head>
<body>
	<div id="wrap" class="container_24">
		<div class="grid_24">
			<%@ include file="../theme/header.jspf" %>
		</div>
		<div id="dialog-confirm" title="Delete this image?">
			<p>Você realmente deseja deletar essa imagem?</p>
		</div>
		<form action="#" method="post" id="mainForm">
			<div class="grid_12">
				<div class="box_top">
					<h2 class="icon time">Informações Gerais</h2>
				</div>
				<div class="box_content padding">
					<input type="hidden" name="place.id" value="${place.id}">
					<div class="field">
						<label class="left">Nome</label>
						<input type="text" class="validate" name="name" value="${place.name}">
					</div>
					<div class="field">
						<label class="left">Telefone</label>
						<input type="text" name="phone" class="validate" value="${place.phoneList[0].number}">
					</div>
					<div class="field">
						<label>Descrição</label>
						 <textarea class="wysiwyg" name="description" rows="4"></textarea>
					</div>
				</div>
			</div>
			<div class="grid_12">
				<div class="box_top">
					<h2 class="icon time">Endereço</h2>
				</div>
				<div class="box_content padding">
					<div class="field">
						<label class="left">Estado</label>
						<input type="text"  id="state" class="validate" value="${place.address.city.state.name}">
						<input name="state" type="hidden" />
					</div>
					<div class="field">
						<label class="left">Cidade</label>
						<input type="text" id="city" class="validate" value="${place.address.city.name}">
					    <input type="hidden" name="city" />
					</div>
					<div class="field">
						<label class="left">CEP</label>
						<input type="text" name="cep" class="small validate" value="${place.address.cep}">
					</div>
					<div class="field">
						<label class="left">Bairro</label>
						<input type="text" name="neighborhood" class="validate" value="${place.address.neighborhood}"> 
					</div>
					<div class="field">
						<label class="left">Rua</label>
						<input type="text" name="street" class="validate" value="${place.address.street}">
					</div>
					<div class="field">
						<label class="left">Numero</label>
						<input type="text" name="number" class="small validate" value="${place.address.number}">
					</div>
					<div class="field">
						<button>Submit</button>
						<button class="secondary" type="reset">Reset</button>
					</div>
				</div>
			</div>
			<div class="grid_24">
				<div class="box_top">
					<h2 class="icon pictures">Imagens</h2>
				</div>
				<div class="box_content">
					<div class="content" id="upEditGallery"></div>
					<ul class="gallery small" id="test"></ul>
				</div>
			</div>
		</form>
		<%@ include file="../theme/footer.jspf" %>
	</div>
</body>
</html>