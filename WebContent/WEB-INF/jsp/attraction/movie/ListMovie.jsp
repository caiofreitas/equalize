<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="../../theme/head.jspf" %>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>${pageName}</title>
</head>
<body>
	<div id="wrap" class="container_24">
		<div class="grid_24">
			<%@ include file="../../theme/header.jspf"%>
		</div>
		<div class="grid_24">
			<div class="box_top">
				<h2 class="icon time">${pageName}</h2>
			</div>
			<div class="box_content">
				<div class="tabs">
					<div id="listing">
						<table class="sorting">
							<thead>
								<tr>
									<th class="checkers"><input type="checkbox"	class="checkall" /></th>
									<th class="align_left">#</th>
									<th class="align_left center">Nome</th>
									<th class="align_left center">Imagem</th>
									<th class="align_left center">Elenco</th>
									<th class="align_left center">Diretor</th>
									<th class="align_left center">Sinopse</th>
									
									<th class="align_left center tools">Tools</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="movie" items="${movies}" varStatus="i">
									<tr>
										<td class="checkers"><input type="checkbox" /></td>
										<td class="align_left">${i.count}</td>
										<td class="align_left center"><a href="#">${movie.name}</a></td>
										<td class="align_left center"><a href="<c:url value="/image/${movie.imageList[0].id}" />"><img alt="img" width="150px;" src="<c:url value="/image/${movie.imageList[0].id}" />" /></a></td>
										<td class="align_left center">${movie.cast}</td>
										<td class="align_left center">${movie.director}</td>
										<td class="align_left center">${movie.synopsis}</td>
										<td class="align_left tools center">
											<a href='<c:url value="/movie/${movie.id}"/>' class="edit tip" title="editar">editar</a>
											<a href='<c:url value="/movie/remove/${movie.id}"/>' class="delete tip" title="deletar">deletar</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<div class="table_actions">
							<input type="checkbox" class="checkall" /> <select>
								<option>Escolha a ação</option>
								<option>Deletar</option>
								<option>Editar</option>
							</select>
							<button class="left">Aplicar nos selecionados</button>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="../../theme/footer.jspf"%>
		</div>
	</div>
</body>
</html>