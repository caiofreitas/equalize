<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="../../theme/head.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${pageName}</title>
</head>
<body>




	<!-- Start: Page Wrap -->
	<div id="wrap" class="container_24">

		<!-- Header Grid Container: Start -->
		<div class="grid_24">

			<%@ include file="../../theme/header.jspf"%>

		</div>

		<!-- 100% Box Grid Container: Start -->
		<div class="grid_24">


			<!-- Box Header: Start -->
			<div class="box_top">

				<h2 class="icon time">${pageName}</h2>

			</div>
			<!-- Box Header: End -->

			<!-- Box Content: Start -->
			<div class="box_content">

				<!-- News Table Tabs: Start -->
				<div class="tabs">

					<!-- News Sorting Table: Start -->
					<div id="listing">

						<table class="sorting">
							<thead>
								<tr>
									<th class="checkers"><input type="checkbox"	class="checkall" /></th>
									<th class="align_left">#</th>
									<th class="align_left center">Nome</th>
									<th class="align_left center">Imagem</th>
									<th class="align_left center">Elenco</th>
									<th class="align_left center">Diretor</th>
									<th class="align_left center">Sinopse</th>
									
									<th class="align_left center tools">Tools</th>
								</tr>
							</thead>
							<tbody>

								<c:forEach var="play" items="${plays}" varStatus="i">
									<tr>
										<td class="checkers"><input type="checkbox" /></td>
										<td class="align_left">${i.count}</td>
										<td class="align_left center"><a href="#">${play.name}</a></td>
										<td class="align_left center"><a href="<c:url value="/images/${play.imageList[0].id}" />"><img alt="img" width="150px;" src="<c:url value="/images/${play.imageList[0].id}" />" /></a></td>
										<td class="align_left center">${play.cast}</td>
										<td class="align_left center">${play.director}</td>
										<td class="align_left center">${play.synopsis}</td>
										<td class="align_left tools center">
											<a href='<c:url value="/play/${play.id}"/>' class="edit tip" title="editar">editar</a> 
											<a href='<c:url value="/play/remove/${play.id}"/>'	class="delete tip" title="deletar">deletar</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>

						<!-- News Sorting Table Actions: Start -->
						<div class="table_actions">
							<input type="checkbox" class="checkall" /> <select>
								<option>Escolha a ação</option>
								<option>Deletar</option>
								<option>Editar</option>
							</select>

							<button class="left">Aplicar nos selecionados</button>
						</div>
						<!-- News Sorting Table Actions: End -->

					</div>
					<!-- News Sorting Table: End -->

				</div>
				<!-- Box Content: End -->
			</div>
			<!-- 100% Box Grid Container: End -->

			<%@ include file="../../theme/footer.jspf"%>

		</div>
	</div>

</body>
</html>