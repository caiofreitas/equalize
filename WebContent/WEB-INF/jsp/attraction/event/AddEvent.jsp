<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="../../theme/head.jspf" %>
	<!-- File uploader -->
	<script type='text/javascript' src='<c:url value="/resources/js/fileuploader.js"/>'></script>
	<!-- uploadeditgallery -->
	<script type='text/javascript' src='<c:url value="/resources/js/uploadeditgallery.js"/>'></script>
	<link rel='stylesheet' href='<c:url value="/resources/css/uploadeditgallery.css"/>' type='text/css' media='screen' />
	<script type="text/javascript">
		$(function(){
			$("#upEditGallery").upEditGallery({'APP_CONTEXT' : APP_CONTEXT });
		});
	</script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>${pageName}</title>
</head>
<body>
	<div id="wrap" class="container_24">
		<div class="grid_24">
			<%@ include file="../../theme/header.jspf" %>
		</div>
		<div id="dialog-confirm" title="Delete this image?">
			<p>Você realmente deseja deletar essa imagem?</p>
		</div>
		<form action="#" method="post" id="mainForm">
			<div class="grid_12">
				<div class="box_top">
					<h2 class="icon time">Informações Gerais</h2>
				</div>
				<div class="box_content padding">
					<input type="hidden" name="movie.id" value="${event.id}">
					<div class="field">
						<label class="left">Nome</label>
						<input type="text" class="validate" name="name" value="${event.name}">
					</div>
					<div class="field">
						<label class="left">Produtor</label>
						<input type="text" name="producer" class="validate" value="${event.producer}">
					</div>
					<div class="field">
						<label>Descrição</label>
						 <textarea class="wysiwyg" name="description" rows="4">${event.description}</textarea>
					</div>
					<div class="field">
						<button>Submit</button>
						<button class="secondary" type="reset">Reset</button>
					</div>
				</div>
			</div>
		
			<div class="grid_12">
				<div class="box_top">
					<h2 class="icon pictures">Imagens</h2>
				</div>
				<div class="box_content">
					<div class="content" id="upEditGallery"></div>
					<ul class="gallery small" id="test"></ul>
				</div>
			</div>
		</form>
		<%@ include file="../../theme/footer.jspf" %>
	</div>
</body>
</html>