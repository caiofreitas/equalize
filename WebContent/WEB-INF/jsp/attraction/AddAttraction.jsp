<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="../theme/head.jspf"%>
	<!-- File uploader -->
	<script type='text/javascript' src='<c:url value="/resources/js/fileuploader.js"/>'></script>
	<!-- uploadeditgallery -->
	<script type='text/javascript' src='<c:url value="/resources/js/uploadeditgallery.js"/>'></script>
	<link rel='stylesheet' href='<c:url value="/resources/css/uploadeditgallery.css"/>' type='text/css' media='screen' />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script type="text/javascript">
		$(function() {
			$( "#event" ).autocomplete({
				source: function( request, response ) {
					$.ajax({
						url: APP_CONTEXT + "event.json",
						dataType: "json",
						data: {
							term: request.term
						},
						success: function( data ) {
							response( $.map( data, function( item ) {
								return {
									label: item.label ,
									value: item.label ,
									id : item.value
								}
							}));
						}
					});
				},
				select: function( event, ui ) {
					var label = "<li><input type='text' class='validate_success' disabled='disabled' value='"+ ui.item.label +"' /><a class='remove' href='#'>remover</a>";
					var input = "<input type='hidden' name='' value='"+ ui.item.id +"' /></li>";
					$("#event").parent().find("ol").append(label + input);
				},	
				minLength: 2
			});
	
			$( "#movie" ).autocomplete({
				source: function( request, response ) {
					$.ajax({
						url: APP_CONTEXT + "movie.json",
						dataType: "json",
						data: {
							term: request.term
						},
						success: function( data ) {
							response( $.map( data, function( item ) {
								return {
									label: item.label ,
									value: item.label ,
									id: item.value
								}
							}));
						}
					});
				},
				select: function( event, ui ) {
					var label = "<li><input type='text' class='validate_success' disabled='disabled' value='"+ ui.item.label +"' /><a class='remove' href='#'>remover</a>";
					var input = "<input type='hidden' name='' value='"+ ui.item.id +"' /></li>";
					
					$("#movie").parent().find("ol").append(label + input);
				},	
				minLength: 2
			});
			
			$( "#play" ).autocomplete({
				source: function( request, response ) {
					$.ajax({
						url: APP_CONTEXT + "play.json",
						dataType: "json",
						data: {
							term: request.term
						},
						success: function( data ) {
							response( $.map( data, function( item ) {
								return {
									label: item.label ,
									value: item.label ,
									id : item.value
								}
							}));
						}
					});
				},
				select: function( event, ui ) {
					var label = "<li><input type='text' class='validate_success' disabled='disabled' value='"+ ui.item.label +"' /><a class='remove' href='#'>remover</a>";
					var input = "<input type='hidden' name='' value='"+ ui.item.id +"' /></li>";
					
					$("#play").parent().find("ol").append(label + input);
				},	
				minLength: 2
			});
			
			$( "#band" ).autocomplete({
				source: function( request, response ) {
					$.ajax({
						url: APP_CONTEXT + "band.json",
						dataType: "json",
						data: {
							term: request.term
						},
						success: function( data ) {
							response( $.map( data, function( item ) {
								return {
									label: item.label ,
									value: item.label ,
									id : item.value
								}
							}));
						}
					});
				},
				select: function( event, ui ) {
					var label = "<li><input type='text' class='validate_success' disabled='disabled' value='"+ ui.item.label +"' /><a class='remove' href='#'>remover</a>";
					var input = "<input type='hidden' name='' value='"+ ui.item.id +"' /></li>";
					
					$("#band").parent().find("ol").append(label + input);
				},	
				minLength: 2
			});
			
			$( "#place" ).autocomplete({
				source: function( request, response ) {
					$.ajax({
						url: APP_CONTEXT + "place.json",
						dataType: "json",
						data: {
							term: request.term
						},
						success: function( data ) {
							response( $.map( data, function( item ) {
								return {
									label: item.label ,
									value: item.label ,
									id : item.value
								}
							}));
						}
					});
				},
				select: function( event, ui ) {
					var label = "<li><input type='text' class='validate_success' disabled='disabled' value='"+ ui.item.label +"' /><a class='remove' href='#'>remover</a>";
					var input = "<input type='hidden' name='' value='"+ ui.item.id +"' /></li>";
					
					$("#place").parent().find("ol").append(label + input);
				},	
				minLength: 2
			});
			
			$(".remove").live("click",function(){
				$(this).parent().remove();
				return false;
			});
			
			$("#upEditGallery").upEditGallery({ 'APP_CONTEXT' : APP_CONTEXT });
		});
	
	</script>
	<title>${pageName}</title>
</head>
<body>
	<div id="wrap" class="container_24">
		<div class="grid_24">
			<%@ include file="../theme/header.jspf"%>
		</div>
		<div id="dialog-confirm" title="Delete this image?">
			<p>Você realmente deseja deletar essa imagem?</p>
		</div>
		<form action="#" method="post" id="mainForm">
			<div class="grid_12">
				<div class="box_top">
					<h2 class="icon time">Informações Gerais</h2>
				</div>
				<div class="box_content padding">
					<input type="hidden" name="id" value="${attraction.id}">
					<div class="field">
						<label class="left">Nome</label> 
						<input type="text" class="validate" name="name" value="${attraction.name}">
					</div>
					<div class="field">
						<label class="left">Faixa etária</label> 
						<input type="text" name="ageRange" class="ageRange" value="${attraction.ageRange}">
					</div>
					<div class="field">
						<label class="left">Telefone</label> 
						<input type="text" name="phone" class="phone" value="${attraction.phone}">
					</div>
					<div class="field">
						<label class="left">Data</label> 
						<input type="text" name="attraction.date" class="validate date" value="${date}">
					</div>
					<div class="field">
						<label class="left">Local</label> 
						<input type="text" id="place" />
					</div>
					<div class="field">
						<label class="left">Eventos</label>
						<input type="text" id="event" />
						<ol></ol>
					</div>
					<div class="field">
						<label class="left">Filmes</label> 
						<input type="text" id="movie" />
						<ol></ol>
					</div>
					<div class="field">
						<label class="left">Peças</label> 
						<input type="text" id="play" />
						<ol></ol>
					</div>
					<div class="field">
						<label class="left">Bandas</label>
						<input type="text" id="band" />
						<ol></ol>
					</div>
					<div class="field">
						<button>Submit</button>
						<button class="secondary" type="reset">Reset</button>
					</div>
				</div>
			</div>
		
			<div class="grid_12">
				<div class="box_top">
					<h2 class="icon pictures">Imagens</h2>
				</div>
				<div class="box_content">
					<div class="content" id="upEditGallery"></div>
				</div>
			</div>
		</form>
		<%@ include file="../theme/footer.jspf"%>
	</div>
</body>
</html>