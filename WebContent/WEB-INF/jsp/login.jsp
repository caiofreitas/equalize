<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="theme/head.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${pageName}</title>
</head>
<body>
	
<!-- Start: Page Wrap -->
<div id="wrap" class="container_24">

	<!-- Header Grid Container: Start -->
	<div class="login">
		
	<!-- Info Notice: Start -->
	
	<c:if test="${msg ne null }">
	
		<div class="notice info">
			<p><b>Atenção </b>  ${msg}.</p>
		</div>
	
	</c:if>
	

	<!-- Info Notice: End -->
	
		<!-- Header: Start -->
		<div id="header">
				
			<!-- Logo: Start -->
			<a href="#" id="logo">Equalize - Sistema Administrativo</a>
			<!-- Logo: End -->
			
			<!-- Login: Start -->
			<form action="<c:url value='/login' />" method="post" id="login">
				
				<!-- Username Input: Start -->
				<label for="username" class="label_username">User</label>
				<input type="text" name="email" id="user" class="password tip-stay validate" title="Entre com o nome do usuário" />
				<!-- Username Input: End -->
				
				<!-- Password Input: Start -->
				<label for="password" class="label_password">Password</label>
				<input type="password" name="password" id="password" class="password tip-stay validate" title="Entre com a senha" />
				<!-- Password Input: End -->
				
				<!-- Login Button: Start -->
				<input type="submit" class="tip" title="Login" />
				<!-- Login Button: End -->
				
			</form>
			<!-- Login: End -->
			
		</div>
		<!-- Header: End -->
		
		<!-- Breadcrumb Bar: Start -->
		<div id="breadcrumb">
			
			
		</div>
		<!-- Breadcrumb Bar: End -->
		
	</div>
	<!-- Header Grid Container: End -->
	
	<!-- End: Page Wrap -->

	
	<!-- jQuery libs - Rest are found in the head section (at top) -->
	<script type="text/javascript" src="<c:url value='resources/js/jquery.visualize-tooltip.js' />"></script>
	<script type="text/javascript" src="<c:url value='resources/js/jquery-animate-css-rotate-scale.js' />"></script>
	<script type="text/javascript" src="<c:url value='resources/js/jquery-ui-1.8.13.custom.min.js' />"></script>
	<script type="text/javascript" src="<c:url value='resources/js/jquery.poshytip.min.js' />"></script>
	<script type="text/javascript" src="<c:url value='resources/js/jquery.quicksand.js' />"></script>
	<script type="text/javascript" src="<c:url value='resources/js/jquery.dataTables.min.js' />"></script>
	<script type="text/javascript" src="<c:url value='resources/js/jquery.facebox.js' />"></script>
	<script type="text/javascript" src="<c:url value='resources/js/jquery.uniform.min.js' />"></script>
	<script type="text/javascript" src="<c:url value='resources/js/jquery.wysiwyg.js' />"></script>
	<script type="text/javascript" src="<c:url value='resources/js/syntaxHighlighter/shCore.js' />"></script>
	<script type="text/javascript" src="<c:url value='resources/js/syntaxHighlighter/shBrushXml.js' />"></script>
	<script type="text/javascript" src="<c:url value='resources/js/syntaxHighlighter/shBrushJScript.js' />"></script>
	<script type="text/javascript" src="<c:url value='resources/js/syntaxHighlighter/shBrushCss.js' />"></script>
	<script type="text/javascript" src="<c:url value='resources/js/syntaxHighlighter/shBrushPhp.js' />"></script>
	<script type="text/javascript" src="<c:url value='resources/js/fileTree/jqueryFileTree.js' />"></script> <!-- Added in 1.2 -->
	
	<!-- jQuery Customization -->
	<script type="text/javascript" src="<c:url value='resources/js/custom.js' />"></script>

</div>
</body>
</html>