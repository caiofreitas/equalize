<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="theme/head.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${pageName}</title>
</head>
<body>

<!-- Start: Page Wrap -->
<div id="wrap" class="container_24">

	
	<!-- Header Grid Container: Start -->
	<div class="grid_24">
		<%@ include file="theme/header.jspf" %>
	</div>
	
	<!-- 25% Box Grid Container: Start -->
	<div class="grid_6">
	
		<%@ include file="theme/menu.jspf" %>
		
	</div>
	<!-- 25% Box Grid Container: End -->
	


	<!-- 100% Box Grid Container: Start -->
	<div class="grid_18">
	
	
		<!-- Box Header: Start -->
		<div class="box_top">
		
			<h2 class="icon time">Atalhos</h2>
		
		</div>
		<!-- Box Header: End -->
	
		<!-- Box Content: Start -->
		<div class="box_content">
		
			<p class="center">
				<!-- List of big icons for quicklinks -->
				<a href="<c:url value="/attraction" />" class="big_button add_news"><span>Atração</span></a>
				<a href="<c:url value="/event" />" class="big_button upload"><span>Novo Evento</span></a>
				<a href="<c:url value="/band" />" class="big_button add_user"><span>Nova Banda</span></a>
				<a href="<c:url value="/movie" />" class="big_button add_event"><span>Novo Filme</span></a>
				<a href="<c:url value="/play" />" class="big_button new_pm popup"><span>Nova Peça</span></a>
				<a href="<c:url value="/place" />" class="big_button support"><span>Novo Local</span></a>
				<a href="<c:url value="user" />" class="big_button add_user"><span>Novo Usuario</span></a>
			</p>
			
		</div>
		<!-- Box Content: End -->
	</div>
	<!-- 100% Box Grid Container: End -->

	<%@ include file="theme/footer.jspf" %>

</div>

</body>
</html>