<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="../theme/head.jspf" %>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>${pageName}</title>
</head>
<body>
	<div id="wrap" class="container_24">
		<div class="grid_24">
			<%@ include file="../theme/header.jspf" %>
		</div>
		<div class="grid_24">
			<div class="box_top">
				<h2 class="icon time">${pageName}</h2>
			</div>
			<div class="box_content">
				<div class="tabs">
					<div id="listing">
						<table class="sorting">
							<thead>
								<tr>
									<th class="checkers"><input type="checkbox" class="checkall" /></th>
									<th class="align_left">#</th>
									<th class="align_left center">Nome</th>
									<th class="align_left center">Login</th>
									<th class="align_left center">Email</th>
									<th class="align_left center tools">Tools</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="user" items="${users}" varStatus="i">
									<tr>
										<th class="checkers"><input type="checkbox" /></th>
										<th class="align_left">${i.count}</th>
										<td class="align_left center"><a href="#">${user.name}</a></td>
										<td class="align_left center"><a href="#" >${user.nickName}</a></td>
										<td class="align_left center">${user.email}</td>
										<td class="align_left tools center">
											<a href='<c:url value="/user/${user.id}"/>' class="edit tip" title="editar">editar</a>
											<a href="<c:url value="/user/remove/${user.id}" />" class="delete tip" title="deletar">deletar</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table> 
						<div class="table_actions">
							<input type="checkbox" class="checkall" />
							<select>
								<option>Escolha a ação</option>
								<option>Deletar</option>
								<option>Editar</option>
							</select>
							<button class="left">Plicar nos selecionados</button>
						</div>
					</div>
				</div>
			
			</div>
		</div>
		<%@ include file="../theme/footer.jspf" %>
	</div>
</body>
</html>