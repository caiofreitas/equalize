
function updateImgs() {
	var data = new Date();
	$(".gallery").find("img").each(function() {
		var no_cache = $(this).attr("src") + "?no_cahe=" + data.getTime();
		$(this).attr("src", no_cache);
		$(".pictures").find("span").html($(".gallery").find("img").length);
	});
}

function showCoords(c) {
	$('#x').val(c.x);
	$('#y').val(c.y);
	$('#x2').val(c.x2);
	$('#y2').val(c.y2);
	$('#w').val(c.w);
	$('#h').val(c.h);
}

var jcrop;
var isToCrop = true;

(function($) {

	$.fn.upEditGallery = function(options) {
		
		var defaults = {
			UPLOAD_URL : 'upload',
			CROP_URL : 'image/crop',
			SAVE_URL : 'image/save',
			TMP_IMG_URL : 'image/tmp',
			ROTATE_RIGHT_URL : 'image/rotate-right',
			ROTATE_LEFT_URL : 'image/rotate-left',
			APP_CONTEXT : '/',
			FORM_ID : 'mainForm',
			USING_WITH_INNER_COLORBOX : false,
			COLORBOX_PARENT_DIV_CONTENT : ""
		};
		
		var settings = $.extend({}, defaults, options);
		
		return this.each(function() {
			var wrapper = 
				'<!-- Delete Dialog: Start -->' +
				'<div id="ue-gallery-dialog-confirm" title="Atenção" style="display: none;">' +
				'	<p>Você realmente deseja remover essa imagem ?</p>' +
				'</div>' +
				'<div id="uploadConteiner">' +
				'<span id="progress"></span>' +
				'	<div id="progressbar"></div>' +
				'	<div id="file-uploader">' +
				'	    <noscript>' +
				'	        <p>Please enable JavaScript to use file uploader.</p>' +
				'	        <!-- or put a simple form for upload here -->' +
				'	    </noscript>' +
				'	</div>' +
				'</div>' ;
			$(this).html(wrapper);
			
			if (settings.USING_WITH_INNER_COLORBOX) {
				$("body").append("<div id='up-edit-gallery-imgs'></div>");
			}
			
			//Ajax uploader
			var uploader = new qq.FileUploader({
			    // pass the dom node (ex. $(selector)[0] for jQuery users) 
			    element: document.getElementById('file-uploader'),
			    // path to server-side upload script 
			    action: settings.APP_CONTEXT + settings.UPLOAD_URL,
			    // additional data to send, name-value pairs 
			    params: {},
			    // validation
			    // ex. ['jpg', 'jpeg', 'png', 'gif'] or [] 
			    allowedExtensions: ['jpg', 'jpeg', 'png'],
			    // each file size limit in bytes 
			    // this option isn't supported in all browsers 
			    sizeLimit: 0, // max size 
			    minSizeLimit: 0, // min size 
			    // set to true to output server response to console 
			    debug: false,
			    // events 
			    // you can return false to abort submit 
			    onSubmit: function(id, fileName){},
			    onProgress: function(id, fileName, loaded, total){
			    	var res = 100*(loaded/total);
			    	$( "#progressbar" ).progressbar( "option", "value", res );
			    	$( "#progressbar" ).progressbar( "option", "value", res );
			    	$( "#progress").html(parseInt(res) + "%");
			    },
			    onComplete: function(id, fileName, responseJSON){
			    	var url = settings.APP_CONTEXT +  settings.TMP_IMG_URL + "/" + responseJSON.fileName;
			    	editImage(url, responseJSON.fileName, responseJSON.image.height , responseJSON.image.width);
			    	$( "#progressbar" ).progressbar( "option", "value", 0 );
			    	
			    	$( "#progress").html("");
			    },
			    onCancel: function(id, fileName){},
			    messages: {
			        // error messages, see qq.FileUploaderBasic for content 
			    },
			    showMessage: function(message){ alert(message); }
			});
			
			
			function editImage(url, imgName, height, width){
				var nocache = new Date();
				
				var dialog = '<div id="dialog-crop" title="Atenção" style="display: none; z-index: 50000;" >' +
					'<p>' +
						'<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>' +
						'Por favor selecione uma região para recortar .' +
					'</p>' +
				'</div>';
				
				var from = dialog + "<form action='" + settings.APP_CONTEXT  + settings.CROP_URL +"' id='cropForm'  method='post' >" +
									"<input type='hidden' id='x' name='x' value='0' /> " +
									"<input type='hidden' id='y' name='y' value='0' /> " +
									"<input type='hidden' id='w' name='w' value='0' /> " +
									"<input type='hidden' id='h' name='h' value='0' /> " +
									"<div id='edit-toolbar'>" +
									"<button class='edit-button crop-btn button-blue' name='rec' value='Recortar' > Recortar </button>" +
									"<button class='edit-button save button-green' name='rec' value='Salvar' > Salvar </button>" +
									"</div>" +
									"<input type='hidden' id='imgName' value='" + url +"' name='imgName' />" +
							"</form>";
				
				$.colorbox({
					html: true,
					html: from + "<img class='crop' src='"+ url +"?no_cahe="+nocache.getTime()+"' alt='' >", 
					width: width + 100, 
					height: height + 150,
					
					onCleanup: function () {

					},
					
					onClosed: function() {
						if (settings.USING_WITH_INNER_COLORBOX) {
							$.colorbox({
								inline : true,
								href : settings.COLORBOX_PARENT_DIV_CONTENT
							});
							
						} else {
							$.colorbox.close();
						}
				    }
				});
				
			}
			
			$("#progressbar").progressbar({ value: 0 });
			
			$(".edit-button.save").live("click", function(){
				
				var data = $("#cropForm").serialize();
				var imgName = $("#imgName").attr("value");
				var imgInput = "<input type='hidden' value='"+ imgName +"' name='image' />";
				var url = "";
				
				var w  = parseInt(jQuery('#w').val());
				if ( w <= 0) {
					url = settings.APP_CONTEXT + settings.SAVE_URL;
				} else {
					url = settings.APP_CONTEXT + settings.CROP_URL;
				}
				
				$.ajax({
					  type: 'POST',
					  url: url,
					  data: data,
					  success: function(){
						  addGalleryItem(imgInput, imgName);
						  if (settings.USING_WITH_INNER_COLORBOX) {
							  $.colorbox({
								  inline : true,
								  href : settings.COLORBOX_PARENT_DIV_CONTENT
							  });
						  } else {
							  $.colorbox.close();
						  }
					  },
						error : function(jqXHR, textStatus, errorThrown){
							alert(textStatus + "</br> -" + errorThrown);
						}
				});
				return false;
			});
			
			
			function getRandom() {
				var dim = jcrop.getBounds();
				return [
					Math.round(Math.random() * dim[0]),
					Math.round(Math.random() * dim[1]),
					Math.round(Math.random() * dim[0]),
					Math.round(Math.random() * dim[1])
				];
			};
			
			$(".edit-button.crop-btn").live("click", function() {
				if(isToCrop) {
					jcrop =  $.Jcrop('.crop');
					jcrop.setOptions({
			            onSelect: showCoords,
			            onChange: showCoords,
			            bgColor: 'black',
			            bgOpacity: .4,
			            aspectRatio: 16/9
			        });
					jcrop.setSelect(getRandom());
					isToCrop = false;
				} else {
					jcrop.destroy();
					$('#w').val(0);
					isToCrop = true;
				}
				return false;
			});
			
			function addGalleryItem(imgInput, imgName) {
				  var gelleryItem =
						'<li>' +
						'	<div class="actions">' +
						'		<a class="delete">delete</a>' +
						'		<a class="view image" href="'+ imgName +'" >view</a>' +
						'	</div>' +
						'	<img width="140px" class="galleryItem" style="opacity: 1;" src="'+ imgName +'" alt="" >' +
						'</li>' +
						imgInput;
				  jQuery(".gallery").append(gelleryItem);
				  jQuery("ul.gallery a.image").colorbox({
					  rel: 'view',
					  photo : true,
					  onClosed: function() {
						  if (settings.USING_WITH_INNER_COLORBOX) {
							  $.colorbox({
								  inline : true,
								  href : settings.COLORBOX_PARENT_DIV_CONTENT
							  });
						} else {
							$.colorbox.close();
						}
				    }
				  });
			}
			
			$("ul.gallery li").live("mouseover", function() {
				$(this).find("img").css("opacity","0.5");
				$(this).find(".actions").show();
				var gelleryItem = (this);
				
				// Se o ícone "x" for pressionado, mostrar confirmação (# dialog-confirm)
				$(this).find(".actions .delete").click(function () {
					
					// confirmação
					$("#ue-gallery-dialog-confirm").dialog({
						resizable: false,
						modal: true,
						minHeight: 0,
						draggable: false,
						zIndex: 50000,
						buttons: {
							//Remove diálogo se o botão cancel é pressionado
							Cancel: function() {
								$(this).dialog("close");
							},
							"Remover": function() {
								$(this).dialog( "close" );
									// Remove imagem se o botão remover for pressionado
									$(gelleryItem).fadeOut('slow', function() { 
										$(gelleryItem).remove(); 
								});
							}
						}
					});
					return false;
				});
				
				
				// Se o ícone "x" for pressionado, mostrar confirmação (# dialog-confirm)
				$(this).find(".actions .delete.presisted-img").click(function () {
					// confirmação
					$("#ue-gallery-dialog-confirm").dialog({
						resizable: false,
						modal: true,
						minHeight: 0,
						draggable: false,
						zIndex: 50000,
						buttons: {
							//Remove diálogo se o botão cancel é pressionado
							Cancel: function() {
								$(this).dialog("close");
							},
							"Remover": function() {
								$(this).dialog( "close" );
								
								var imgId = $(gelleryItem).find(".presisted-img-id").val();
								
								$("#" + settings.FORM_ID).append("<input type='hidden' name='imagesToRemove' value='" + imgId +  "' />");
								
									// Remove imagem se o botão remover for pressionado
									$(gelleryItem).fadeOut('slow', function() { 
										$(gelleryItem).remove(); 
								});
							}
						}
					});
					return false;
				});
				
			  });
			
			$("ul.gallery li").live("mouseout", function(){
				$(this).find("img").css("opacity","1");
				$(this).find(".actions").hide();
			});
		});
	};
	
})(jQuery);
