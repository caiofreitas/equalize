
	//-------------------------------------------------------------- */
	// Insere "v"'s para dropdowns antes do documento carregar
	//-------------------------------------------------------------- */
	
	// insere o "v" Icon
	$("<span class='v'></span>").insertBefore("#userpanel ul.dropdown ul.subnav");
		
	// insere o "v" Icon
	$("<span class='v'></span>").insertAfter("ul#navigation.dropdown ul.subnav");

(function ($){
$(document).ready(function() {
	//-------------------------------------------------------------- */
	// Dropdown z-index Fix for IE7 
	//-------------------------------------------------------------- */
	$(function() {
		var zIndexNumber = 1000;
		$('.ie7 #wrap div').each(function() {
			$(this).css('zIndex', zIndexNumber);
			zIndexNumber -= 10;
		});
	});
	//-------------------------------------------------------------- */
	// Box Slider
	//-------------------------------------------------------------- */

	// Quando top box é clicado, ele desliza para cima
	$(".box_top h2").click(function(){
		$(this).toggleClass("toggle_closed").parent().next().slideToggle("slow");
		return false; //Prevent the browser jump to the link anchor
	}); 

	//-------------------------------------------------------------- */
	// Box Tabs
	//-------------------------------------------------------------- */

	$('div.tabs').each(function() {

		var $tabs_name = $(this);
		
		// Se não houver uma guia, encontrar o ativo
		if ($(this).parent().prev().children('ul.sorting').length) {
			
			// classificando atalho
			var $tabs_tabs = $(this).parent().prev().children('ul.sorting');
			
			// Se ativo, então mostrá-lo
			if ($($tabs_tabs).children().children('a').hasClass('active')) {
					
				// Localiza o href e remove o #
				var $stats_type = $($tabs_tabs).children().find('.active').attr('href');
					
				$($tabs_tabs).children().children('a').each(function() {
					var $tab_names = $(this).attr('href');
					// oculta todos os
					$($tabs_name).find($tab_names).hide();
				});
					
				$($tabs_name).find($stats_type).show();
			};
	
			// Quando uma das guias é clicada
			$($tabs_tabs).children().children('a').click(function(){
				
				$($tabs_tabs).children().children('a').each(function() {
					var $tab_names = $(this).attr('href');
					// Hides all
					$($tabs_name).find($tab_names).hide();
				});
				
				// Localiza o href e remove o #
				var $tabs_tabs_type = $(this).attr('href');
				
				// desaparece quando clicado
				$($tabs_name).parent().find($tabs_tabs_type).fadeIn('slow');
				
				// remove a classe 'active'
				$($tabs_tabs).children().children('a').removeClass('active');
				
				// Adiciona estado ativo para o que você clicou
				$(this).addClass('active');
				return false; //Evitar o salto navegador para a âncora do link
			}); 
		};	
		
	});

	//-------------------------------------------------------------- */
	// 	A ordenação da tabela (DataTables) http://www.datatables.net/
	//-------------------------------------------------------------- */

	// A ordenação da tabela com todas as funcionalidades (paginação pesquisa, ..)
	$('table.sorting').dataTable( {
		"sPaginationType": "full_numbers",
		"bAutoWidth": false
	} );
	
	// Tabela de classificação com paginação 
	$('table.simple_sorting').dataTable( {
		"sPaginationType": "full_numbers",
		"bFilter": false,
		"bLengthChange": false,
		"bSort": false,
		"bSortClasses": false,
		"bAutoWidth": false
	} );
	
	// Remove informações sobre a paginação, devido a problemas de espaço
//	if ($('.table_actions')) {
//		$('.table_actions').each(function() {
//			$(this).parent().find('.dataTables_info').hide();
//		});	
//	};


	//-------------------------------------------------------------- */
	// 	Dicas (Poshytip) http://vadikom.com/demos/poshytip/
	//-------------------------------------------------------------- */

	// Dica para home icon etc.
	$('.tip').poshytip({
		className: 'tip-theme',
		showTimeout: 1,
		alignTo: 'target',
		alignX: 'center',
		alignY: 'bottom',
		offsetX: 0,
		offsetY: 16,
		allowTipHover: false,
		fade: false,
		slide: false
	});
	
	// Dica que fica
	$('.tip-stay').poshytip({
		className: 'tip-theme',
		showOn:'focus',
		showTimeout: 1,
		alignTo: 'target',
		alignX: 'center',
		alignY: 'bottom',
		offsetX: 0,
		offsetY: 16,
		allowTipHover: false,
		fade: false,
		slide: false
	});

	//-------------------------------------------------------------- */
	// Remove as caixas de aviso quando pressionado
	//-------------------------------------------------------------- */

	// Quando a caixa de aviso é clicada
	$("div.notice, p.error, p.warning, p.info, p.note, p.success").click(function() { 
		$(this).fadeOut('slow');
	});   

	//-------------------------------------------------------------- */
	// validação
	//-------------------------------------------------------------- */

	function validation(){

		// Faz a validação no blur do input 
		$(".validate").blur(function () {
			
			// Se algo não for digitado adiciona o ícone de erro,
			if ($(this).val() === ""){
				$(this).removeClass("validate_success");
		    	$(this).addClass("validate_error");
		    	return false;
			}
			// Se algo for digitado adiciona o ícone sucesso
			else {
				$(this).removeClass("validate_error");
		    	$(this).addClass("validate_success");
		    	return true;
			};
		});
	};

	// Chama a função de validação quando um campo de entrada recebe foco
	$(".validate").focus(function () {
	      return validation();
	});
	

	// Painel de usuáriol Dropdown
	
	// Quando hovering over uma ul com a classe "dropdown"
	$("#userpanel ul.dropdown").hover(function() { 
	  
		// Define o texto superior como ativo (o dropdown)
		$(this).find(".top").addClass("active");
		
		// Desliza o "subnav" em foco
		$(this).find("ul.subnav").show();
		// Mostra o 'v' branco 
		$(this).find("span.v").addClass("active");
	  
		// On hover off
		$(this).hover(function() {  
		}, function(){  
			
			// Esconde o subnavegação quando não está no "subnav" mais
			$(this).find("ul.subnav").stop(false, true).hide(); 
			
			// Remove o texto superior como ativo (o dropdown)
			$(this).find(".top").removeClass("active");
			
			// Esconde o 'v' branco 
			$(this).find("span.v").removeClass("active");
			
		});  
	});  

	// Navegação Dropdown

	// Quando mouse passa sobre um elemento ul com classe "Dropdown"
	$("ul#navigation.dropdown li.topnav").hover(function() { 
	
		// copia o nome de navegação e o ícone "v" para o topo da lista suspensa
		$(this).clone().prependTo("ul#navigation.dropdown .subnav");
		$("ul#navigation.dropdown .subnav .subnav").remove();	
		
		//Desliza o "subnav" em foco
		$(this).find("ul.subnav").show();
		
		// Evento hover off
		$("ul#navigation.dropdown ul.subnav").parent().hover(function() {  
		}, function(){  
			
			// Esconde o subnavegação quando não está mais no "subnav" 
			$(this).find("ul.subnav").stop(false, true).hide(); 
			
			// Remove a parte superior para evitar a duplicação
			$("ul#navigation.dropdown .subnav .topnav").remove();
		});  
	});  
	
 
	//-------------------------------------------------------------- */
	// Ações da Galeria
	//-------------------------------------------------------------- */



	//-------------------------------------------------------------- */
	// Fim das ações da Galeria
	//-------------------------------------------------------------- */


	//	http://plugins.jquery.com/project/jWYSIWYG
	$(".wysiwyg").wysiwyg();
	
	// Select date
	$("input.date").datepicker();
	
	// Inline date
	$(".inlinedate").datepicker();
	
	// 	**** Accordion (jQuery UI) **** 
	$( ".accordion" ).accordion();

	//-------------------------------------------------------------- */
	// 	Elementos de Formulário (uniform) http://uniformjs.com/ 
	//-------------------------------------------------------------- */
	$(".box_content select, .box_content input:checkbox, .box_content input:radio, .box_content input:file, .box_content button").uniform();

	//-------------------------------------------------------------- */
	// Marca todos os checkboxes
	//-------------------------------------------------------------- */
	$('.checkall').click(function () {
		var checkall =$(this).parents('.box_content:eq(0)').find(':checkbox').attr('checked', this.checked);
		$.uniform.update(checkall);
	});
// End jQuery
   });
})(jQuery);
