/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.equalize.model.wrapper;

import java.io.Serializable;

/**
 * 
 * @author caio
 */

public class CityWrapper implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String name;

	private Long stateId;


	public CityWrapper() {
	}

	public CityWrapper(Long id, String name, Long stateId) {
		this.id = id;
		this.name = name;
		this.stateId = stateId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getState() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}


	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof CityWrapper)) {
			return false;
		}
		CityWrapper other = (CityWrapper) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "br.com.letmelnow.model.CityWrapper[ id=" + id + " ]";
	}

}
