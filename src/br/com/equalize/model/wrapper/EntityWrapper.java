package br.com.equalize.model.wrapper;

public class EntityWrapper {

	private Long value;
	private String label;

	public EntityWrapper(Long value, String name) {
		super();
		this.value = value;
		this.label = name;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
