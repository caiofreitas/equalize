/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.equalize.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author caio
 */
@Entity
@Table(name = "t_place")
@NamedQueries({
		@NamedQuery(name = "Place.findAll", query = "SELECT p FROM Place p"),
		@NamedQuery(name = "Place.findById", query = "SELECT p FROM Place p WHERE p.id = :id") })
public class Place implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;
	
	@Column(name = "description")
	private String description;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "place", fetch = FetchType.EAGER)
	private List<Phone> phoneList  = new ArrayList<Phone>();

	@JoinColumn(name = "address_id", referencedColumnName = "id")
	@ManyToOne(cascade = CascadeType.ALL)
	private Address address;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "place", fetch = FetchType.EAGER)
	private List<Image> imageList = new ArrayList<Image>();

	@OneToMany(mappedBy = "place")
	private List<Attraction> attractionList = new ArrayList<Attraction>();

	public Place() {
	}

	public Place(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Phone> getPhoneList() {
		return phoneList;
	}

	public void setPhoneList(List<Phone> phoneList) {
		this.phoneList = phoneList;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<Image> getImageList() {
		return imageList;
	}

	public void setImageList(List<Image> imageList) {
		this.imageList = imageList;
	}

	public List<Attraction> getAttractionList() {
		return attractionList;
	}

	public void setAttractionList(List<Attraction> attractionList) {
		this.attractionList = attractionList;
	}
	
	public void addPhone(Phone phone){
		phone.setPlace(this);
		this.getPhoneList().add(phone);
	}
	
	public void addImage(Image image) {
		image.setPlace(this);
		this.getImageList().add(image);
	}
	
	public void addAttraction(Attraction attraction) {
		attraction.setPlace(this);
		this.getAttractionList().add(attraction);
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Place)) {
			return false;
		}
		Place other = (Place) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "br.com.letmelnow.model.Place[ id=" + id + " name= " + name + "]";
		
		
	}

}
