/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.equalize.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 * @author caio
 */
@Entity
@Table(name = "t_attraction")
@NamedQueries({
		@NamedQuery(name = "Attraction.findAll", query = "SELECT a FROM Attraction a"),
		@NamedQuery(name = "Attraction.findById", query = "SELECT a FROM Attraction a WHERE a.id = :id"),
		@NamedQuery(name = "Attraction.findByAgeRange", query = "SELECT a FROM Attraction a WHERE a.ageRange = :ageRange") })
public class Attraction implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "ageRange")
	private Integer ageRange;

	// TODO: Meta dados.
	@Transient
	private String metaDados;

	@JoinTable(name = "r_attraction_has_movie", joinColumns = { @JoinColumn(name = "attraction_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "movie_id", referencedColumnName = "id") })
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Movie> movieList = new ArrayList<Movie>();

	@JoinTable(name = "r_attraction_has_play", joinColumns = { @JoinColumn(name = "attraction_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "play_id", referencedColumnName = "id") })
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Play> playList = new ArrayList<Play>();

	@JoinTable(name = "r_attraction_has_event", joinColumns = { @JoinColumn(name = "attraction_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "event_id", referencedColumnName = "id") })
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Event> eventList = new ArrayList<Event>();

	@JoinTable(name = "r_attraction_has_band", joinColumns = { @JoinColumn(name = "attraction_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "band_id", referencedColumnName = "id") })
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Band> bandList = new ArrayList<Band>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "attraction", fetch = FetchType.EAGER)
	private List<Phone> phoneList = new ArrayList<Phone>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "attraction", fetch = FetchType.EAGER)
	private List<Image> imageList = new ArrayList<Image>();

	@JoinColumn(name = "classification_id", referencedColumnName = "id")
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Classification classification;

	@JoinColumn(name = "date_id", referencedColumnName = "id")
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private AttractionDate date;

	@JoinColumn(name = "place_id", referencedColumnName = "id")
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Place place;

	public Attraction() {
	}

	public Attraction(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAgeRange() {
		return ageRange;
	}

	public void setAgeRange(Integer ageRange) {
		this.ageRange = ageRange;
	}

	public List<Movie> getMovieList() {
		return movieList;
	}

	public void setMovieList(List<Movie> movieList) {
		this.movieList = movieList;
	}

	public List<Play> getPlayList() {
		return playList;
	}

	public void setPlayList(List<Play> playList) {
		this.playList = playList;
	}

	public List<Event> getEventList() {
		return eventList;
	}

	public void setEventList(List<Event> eventList) {
		this.eventList = eventList;
	}

	public List<Band> getBandList() {
		return bandList;
	}

	public void setBandList(List<Band> bandList) {
		this.bandList = bandList;
	}

	public List<Phone> getPhoneList() {
		return phoneList;
	}

	public void setPhoneList(List<Phone> phoneList) {
		this.phoneList = phoneList;
	}

	public List<Image> getImageList() {
		return imageList;
	}

	public void setImageList(List<Image> imageList) {
		this.imageList = imageList;
	}

	public Classification getClassification() {
		return classification;
	}

	public void setClassification(Classification classification) {
		this.classification = classification;
	}

	public AttractionDate getDate() {
		return date;
	}

	public void setDate(AttractionDate date) {
		this.date = date;
	}

	public Place getPlace() {
		return place;
	}

	public String getMetaDados() {
		return metaDados;
	}

	public void setMetaDados(String metaDados) {
		this.metaDados = metaDados;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public void addMovie(Movie movie) {
		movie.addAttraction(this);
		this.getMovieList().add(movie);
	}

	public void addEvent(Event event) {
		event.addAttraction(this);
		this.getEventList().add(event);
	}

	public void addPlay(Play play) {
		play.addAttraction(this);
		this.getPlayList().add(play);
	}

	public void addBand(Band band) {
		band.addAttraction(this);
		this.getBandList().add(band);
	}

	public void addPhone(Phone phone) {
		phone.setAttraction(this);
		this.getPhoneList().add(phone);
	}

	public void addImage(Image image) {
		image.setAttraction(this);
		this.getImageList().add(image);
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Attraction)) {
			return false;
		}
		Attraction other = (Attraction) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "br.com.letmelnow.model.Attraction[ id=" + id + " ]";
	}

}
