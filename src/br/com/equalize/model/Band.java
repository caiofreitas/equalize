/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.equalize.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author caio
 */
@Entity
@Table(name = "t_band")
@NamedQueries({
		@NamedQuery(name = "Band.findAll", query = "SELECT b FROM Band b"),
		@NamedQuery(name = "Band.findById", query = "SELECT b FROM Band b WHERE b.id = :id") })
public class Band implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "members")
	private String members;

	@Column(name = "producer")
	private String producer;

	@Column(name = "description")
	private String description;

	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "bandList", fetch = FetchType.LAZY)
	private List<Attraction> attractionList = new ArrayList<Attraction>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "band", fetch = FetchType.LAZY)
	private List<Image> imageList = new ArrayList<Image>();
	
	public Band() {
	}

	public Band(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMembers() {
		return members;
	}

	public void setMembers(String members) {
		this.members = members;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Attraction> getAttractionList() {
		return attractionList;
	}

	public void setAttractionList(List<Attraction> attractionList) {
		this.attractionList = attractionList;
	}

	public List<Image> getImageList() {
		return imageList;
	}

	public void setImageList(List<Image> imageList) {
		this.imageList = imageList;
	}


	public void addAttraction(Attraction attraction) {
		attraction.addBand(this);
		this.getAttractionList().add(attraction);
	}
	
	public void addImage(Image image) {
		image.setBand(this);
		this.getImageList().add(image);
	}

	
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Band)) {
			return false;
		}
		Band other = (Band) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "br.com.letmelnow.model.Band[ id=" + id + " ]";
	}

}
