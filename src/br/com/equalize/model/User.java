/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.equalize.model;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author caio
 */
@Entity
@Table(name = "t_user")
@NamedQueries({ @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
		@NamedQuery(name = "User.findById", query = "SELECT u FROM User u WHERE u.id = :id") })
public class User implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Long id;
	
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "name")
	private String name;
	
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "nickName", unique = true)
	private String nickName;
	
	@Column(name = "email")
	private String email;
	
	@Basic(optional = false)
	@Column(name = "password")
	private String password;
	
	@Column(name = "groups")
	private String groups;
	
	@JoinTable(name = "r_user_has_classification", joinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "classification_id", referencedColumnName = "id") })
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
	private List<Classification> classificationList = new ArrayList<Classification>();
	
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "user", fetch = FetchType.EAGER)
	private List<Likes> likesList = new ArrayList<Likes>();
	
	@JoinColumn(name = "address_id", referencedColumnName = "id")
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Address address;
	
	public User() {
	}
	
	public User(Long id) {
		this.id = id;
	}
	
	public User(Long id, String password) {
		this.id = id;
		this.password = password;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getNickName() {
		return nickName;
	}
	
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = getSha1Hash(password);
	}
	
	public boolean passwordMatch(String password) {
		if (getSha1Hash(password).equals(this.password))
			return true;
		else
			return false;
	}
	
	private String getSha1Hash(String password) {
		StringBuilder s = new StringBuilder();
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA1");
			md.update(password.getBytes());
			byte[] hashSha1 = md.digest();
			
			for (int i = 0; i < hashSha1.length; i++) {
				int parteAlta = ((hashSha1[i] >> 4) & 0xf) << 4;
				int parteBaixa = hashSha1[i] & 0xf;
				if (parteAlta == 0)
					s.append('0');
				s.append(Integer.toHexString(parteAlta | parteBaixa));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return s.toString();
	}
	
	public String getGroups() {
		return groups;
	}
	
	public void setGroups(String groups) {
		this.groups = groups;
	}
	
	public List<Classification> getClassificationList() {
		return classificationList;
	}
	
	public void setClassificationList(List<Classification> classificationList) {
		this.classificationList = classificationList;
	}
	
	public List<Likes> getLikesList() {
		return likesList;
	}
	
	public void setLikesList(List<Likes> likesList) {
		this.likesList = likesList;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
	
	public void addClassification(Classification classification) {
		classification.addUser(this);
		this.getClassificationList().add(classification);
	}
	
	public void addLikes(Likes likes) {
		likes.setUser(this);
		this.getLikesList().add(likes);
	}
	
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}
	
	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof User)) {
			return false;
		}
		User other = (User) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", nickName=" + nickName + ", email=" + email + ", password="
				+ password + ", groups=" + groups + ", classificationList=" + classificationList + ", likesList="
				+ likesList + ", address=" + address + "]";
	}
	
}
