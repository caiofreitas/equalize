/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.equalize.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author caio
 */
@Entity
@Table(name = "t_classification")
@NamedQueries({
		@NamedQuery(name = "Classification.findAll", query = "SELECT c FROM Classification c"),
		@NamedQuery(name = "Classification.findById", query = "SELECT c FROM Classification c WHERE c.id = :id") })
public class Classification implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Long id;

	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "classificationList", fetch = FetchType.EAGER)
	private List<User> userList = new ArrayList<User>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "classification", fetch = FetchType.LAZY)
	private List<Attraction> attractionList = new ArrayList<Attraction>();
	
	public Classification() {
	}

	public Classification(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public List<Attraction> getAttractionList() {
		return attractionList;
	}

	public void setAttractionList(List<Attraction> attractionList) {
		this.attractionList = attractionList;
	}
	
	public void addUser(User user) {
		user.addClassification(this);
		this.getUserList().add(user);
	}
	
	public void addAttraction(Attraction attraction){
		attraction.setClassification(this);
		this.getAttractionList().add(attraction);
	}
	
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Classification)) {
			return false;
		}
		Classification other = (Classification) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "br.com.letmelnow.model.Classification[ id=" + id + " ]";
	}

}
