/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.equalize.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author caio
 */
@Entity
@Table(name = "t_attractionDate")
@NamedQueries({
		@NamedQuery(name = "AttractionDate.findAll", query = "SELECT a FROM AttractionDate a"),
		@NamedQuery(name = "AttractionDate.findById", query = "SELECT a FROM AttractionDate a WHERE a.id = :id"),
		@NamedQuery(name = "AttractionDate.findByDate", query = "SELECT a FROM AttractionDate a WHERE a.date = :date") })
public class AttractionDate implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Long id;

	@Column(name = "date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	@OneToMany(mappedBy = "date", fetch = FetchType.LAZY)
	private List<Attraction> attractionList = new ArrayList<Attraction>();
	
	public AttractionDate() {
	}

	public AttractionDate(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<Attraction> getAttractionList() {
		return attractionList;
	}

	public void setAttractionList(List<Attraction> attractionList) {
		this.attractionList = attractionList;
	}

	public void addAttraction(Attraction attraction) {
		attraction.setDate(this);
		this.getAttractionList().add(attraction);
	}
	
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof AttractionDate)) {
			return false;
		}
		AttractionDate other = (AttractionDate) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "br.com.letmelnow.model.AttractionDate[ id=" + id + " ]";
	}

}
