package br.com.equalize.model.transite;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class UploadItem {
	//private String name;
	//private String viewName;
	private CommonsMultipartFile fileData;

//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//	
//	public String getViewName() {
//		return viewName;
//	}
//
//	public void setViewName(String viewName) {
//		this.viewName = viewName;
//	}


	public CommonsMultipartFile getFileData() {
		return fileData;
	}

	public void setFileData(CommonsMultipartFile fileData) {
		this.fileData = fileData;
	}
}
