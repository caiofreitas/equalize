package br.com.equalize.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController extends AbstractController {
	
	@RequestMapping("/")
	public ModelAndView home(HttpSession session) {
		ModelAndView modelAndView;
		String mensagem = "Let Me Know 1.0";
		modelAndView = new ModelAndView("home");
		modelAndView.addObject("mensagem", mensagem);
		return modelAndView;
	}
}
