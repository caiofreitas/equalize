package br.com.equalize.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.equalize.manager.ImageManager;
import br.com.equalize.util.ManipuleImage;

import com.im.imjutil.logging.Logger;
import com.im.imjutil.validation.Convert;

@Controller
@RequestMapping(value = "/image/**")
public class ImageController {
	
	private static String TEMPORARY_DIRECTORY_PATH;
	
	static {
		TEMPORARY_DIRECTORY_PATH = "";
		File tmp;
		try {
			tmp = File.createTempFile("equalize", ".jpg");
			TEMPORARY_DIRECTORY_PATH = tmp.getParent();
			tmp.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping(value = "/image/{id}", method = RequestMethod.GET)
	public void getImage(HttpServletResponse response, @PathVariable("id") long id, ModelMap modelMap) {
		
		response.setContentType("image/jpeg");
		response.setHeader("Content-Disposition", "inline");
		
		try {
			response.getOutputStream().write(ImageManager.dao().get(id).getBytes());
		} catch (IOException e) {
			Logger.error(e.getMessage());
		}
		response.setStatus(HttpServletResponse.SC_OK);
	}
	
	@RequestMapping(value = "/image/tmp/{fileName}", method = RequestMethod.GET)
	public void getImage(HttpServletResponse response, @PathVariable String fileName, ModelMap modelMap) {
		
		response.setContentType("image/jpeg");
		response.setHeader("Content-Disposition", "inline");
		
		try {
			String imageFile = TEMPORARY_DIRECTORY_PATH + File.separator + fileName + ".jpg";
			
			byte readBuf[] = new byte[512 * 1024];
			FileInputStream fin = new FileInputStream(new File(imageFile));
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			int readCnt = fin.read(readBuf);
			
			while (0 < readCnt) {
				bout.write(readBuf, 0, readCnt);
				readCnt = fin.read(readBuf);
			}
			
			fin.close();
			response.getOutputStream().write(bout.toByteArray());
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			Logger.error(e.getMessage());
		}
		response.setStatus(HttpServletResponse.SC_OK);
	}
	
	@RequestMapping(value = "/image/crop", method = RequestMethod.POST)
	public void cropRequest(HttpServletRequest request, HttpServletResponse response) {
		float topLeft_X, topLeft_Y, height, width;
		String fileName;
		topLeft_X = Convert.toFloat(request.getParameter("x"));
		topLeft_Y = Convert.toFloat(request.getParameter("y"));
		height = Convert.toFloat(request.getParameter("h"));
		width = Convert.toFloat(request.getParameter("w"));
		String vet[] = request.getParameter("imgName").split("/");
		fileName = vet[4];
		System.out.println("File name: " + fileName);
		try {
			String imagePath = TEMPORARY_DIRECTORY_PATH + File.separator + fileName;
			File imageFile = new File(imagePath);
			ManipuleImage.cropImageAsJPG(imageFile, topLeft_X, topLeft_Y, width, height);
		} catch (IOException e) {
			Logger.error(e.getMessage());
		}
		response.setStatus(HttpServletResponse.SC_OK);
	}
}
