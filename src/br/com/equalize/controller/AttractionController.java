package br.com.equalize.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.equalize.manager.AttractionManager;
import br.com.equalize.model.Attraction;

@Controller
public class AttractionController extends AbstractController {
	
	@RequestMapping(value = "/attraction", method = RequestMethod.GET)
	public ModelAndView newAttractio(HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "New Attraction";
		modelAndView = new ModelAndView("attraction/AddAttraction");
		modelAndView.addObject("pageName", pageName);
		return modelAndView;
	}
	
	@RequestMapping(value = "/attraction/{id}", method = RequestMethod.GET)
	public ModelAndView editAttractio(@PathVariable long id, HttpSession session) {
		
		ModelAndView modelAndView;
		String pageName = "New Attraction";
		modelAndView = new ModelAndView("attraction/AddAttraction");
		modelAndView.addObject("pageName", pageName);
		modelAndView.addObject("attraction", AttractionManager.dao().get(id));
		return modelAndView;
	}
	
	@RequestMapping(value = "/attraction/list", method = RequestMethod.GET)
	public ModelAndView listPlace(HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "List Attraction";
		modelAndView = new ModelAndView("attraction/ListAttraction");
		modelAndView.addObject("pageName", pageName);
		modelAndView.addObject("attractions", AttractionManager.dao().getAll());
		return modelAndView;
	}
	
	@RequestMapping(value = "/attraction", method = RequestMethod.POST)
	public String addAttraction(Attraction attraction, HttpSession session) {
		AttractionManager.dao().add(attraction);
		return "redirect:attraction/list";
	}
	
}
