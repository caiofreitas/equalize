package br.com.equalize.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.equalize.manager.UserManager;
import br.com.equalize.model.User;

@Controller
public class LoginController {
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public ModelAndView newLogin(HttpServletRequest request) {
		String msg = (String) request.getParameter("msg");
		String pageName = "Login Page";
		ModelAndView modelAndView = new ModelAndView("login");
		modelAndView.addObject("pageName", pageName);
		if (msg != null) {
			modelAndView.addObject("msg", msg);
		}
		return modelAndView;
	}
	
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public String validLogin(User u, HttpSession session) {
		User user = UserManager.login(u.getEmail(), u.getPassword());
		if (user != null) {
			session.setAttribute("user", user);
			return "redirect:";
		} else {
			return "redirect:login?msg=Usario ou senha invalidos";
		}
	}
	
	@RequestMapping(value = "logout")
	public String logout(HttpSession session) {
		session.setAttribute("user", null);
		return "redirect:login";
	}
}
