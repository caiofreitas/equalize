package br.com.equalize.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.equalize.manager.PlayManger;
import br.com.equalize.model.Image;
import br.com.equalize.model.Play;
import br.com.equalize.model.wrapper.EntityWrapper;

@Controller
public class PlayController extends AbstractController {
	
	@RequestMapping(value = "/play", method = RequestMethod.GET)
	public ModelAndView newPlay(HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "New Page";
		modelAndView = new ModelAndView("attraction/play/AddPlay");
		modelAndView.addObject("pageName", pageName);
		return modelAndView;
	}
	
	@RequestMapping(value = "/play/{id}", method = RequestMethod.GET)
	public ModelAndView editPlay(@PathVariable long id, HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "New Page";
		modelAndView = new ModelAndView("attraction/play/AddPlay");
		modelAndView.addObject("pageName", pageName);
		modelAndView.addObject("play", PlayManger.dao().get(id));
		return modelAndView;
	}
	
	@RequestMapping(value = "/play/remove/{id}", method = RequestMethod.GET)
	public String removePlay(@PathVariable long id, HttpSession session) {
		Play play = PlayManger.dao().get(id);
		PlayManger.dao().remove(play);
		return "redirect:/play/list";
	}
	
	@RequestMapping(value = "/play/list")
	public ModelAndView listPlay(HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "List Play";
		modelAndView = new ModelAndView("attraction/play/ListPlay");
		modelAndView.addObject("pageName", pageName);
		modelAndView.addObject("plays", PlayManger.dao().getAll());
		return modelAndView;
	}
	
	@RequestMapping(value = "/play", method = RequestMethod.POST)
	public String addPlay(Play play, HttpServletRequest request, HttpSession session) {
		play.setImageList(getImages(request));
		for (Image img : play.getImageList()) {
			img.setPlay(play);
		}
		PlayManger.dao().add(play);
		return "redirect:play/list";
	}
	
	@RequestMapping(value = "play.json", method = RequestMethod.GET)
	public @ResponseBody
	List<EntityWrapper> getPlaceJson(@RequestParam String term, HttpSession session) {
		List<EntityWrapper> entityWrappers = new ArrayList<EntityWrapper>();
		entityWrappers = PlayManger.findAllEntityWrappers(term);
		return entityWrappers;
	}
}
