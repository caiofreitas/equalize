package br.com.equalize.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.com.equalize.model.Image;

public class AbstractController {
	
	
	private static String TEMPORARY_DIRECTORY_PATH;
	
	static {
		TEMPORARY_DIRECTORY_PATH = "";
		File tmp;
		try {
			tmp = File.createTempFile("equalize", ".jpg");
			TEMPORARY_DIRECTORY_PATH = tmp.getParent();
			tmp.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	protected List<Image> getImages(HttpServletRequest request) {
		
		List<Image> images = new ArrayList<Image>();
		
		String submitedImages[] = request.getParameterValues("image");
		
		if (submitedImages != null)
			
			if (submitedImages.length >= 1) {
				
				for (String item : submitedImages) {
					
					String sub[] = item.split("/");
					
					String imageFile = TEMPORARY_DIRECTORY_PATH + File.separator + sub[4];
					
					try {
						
						byte readBuf[] = new byte[512 * 1024];
						
						FileInputStream fin = new FileInputStream(new File(imageFile));
						
						ByteArrayOutputStream bout = new ByteArrayOutputStream();
						
						int readCnt = fin.read(readBuf);
						while (0 < readCnt) {
							bout.write(readBuf, 0, readCnt);
							readCnt = fin.read(readBuf);
						}
						
						fin.close();
						
						Image img = new Image();
						
						img.setBytes(bout.toByteArray());
						images.add(img);
						
					} catch (FileNotFoundException e) {
						
						e.printStackTrace();
					} catch (IOException e) {
						
						e.printStackTrace();
					}
					
				}
				
				return images;
				
			}
		return images;
		
	}
}
