package br.com.equalize.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.equalize.util.ManipuleImage;

import com.im.imjutil.logging.Logger;
import com.im.imjutil.util.Pair;

/**
 * 
 * @author Caio Freitas
 */
@Controller
public class UploadController {
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		PrintWriter writer = null;
		InputStream is = null;
		FileOutputStream fos = null;
		
		response.setContentType("application/json");
		
		try {
			writer = response.getWriter();
			
			is = request.getInputStream();
			
			File temp = File.createTempFile("equalize_", ".jpg");
			fos = new FileOutputStream(temp);
			fos.write(IOUtils.toByteArray(is));
			fos.close();
			is.close();
			
			ManipuleImage.resizeImageAsJPG(temp, 800);
			
			Pair<Integer, Integer> imageDimensions = ManipuleImage.getImageDimensions(temp);
			
			int width = imageDimensions.getFirst();
			int height = imageDimensions.getLast();
			
			String image = ", \"image\" : { \"height\" : " + height + " , \"width\" : " + width + " }";
			
			response.setStatus(HttpServletResponse.SC_OK);
			writer.print("{\"success\": true, \"fileName\": \"" + temp.getName() + "\" "+ image +" }");
		} catch (FileNotFoundException ex) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			writer.print("{\"success\": false}");
			Logger.error("has thrown an exception: " + ex.getMessage());
		} catch (IOException ex) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			writer.print("{\"success\": false}");
			Logger.error("has thrown an exception: " + ex.getMessage());
		} finally {
			writer.flush();
			writer.close();
		}
	}
	
}