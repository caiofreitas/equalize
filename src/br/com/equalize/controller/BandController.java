package br.com.equalize.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.equalize.manager.BandManager;
import br.com.equalize.model.Band;
import br.com.equalize.model.Image;
import br.com.equalize.model.wrapper.EntityWrapper;

@Controller
public class BandController extends AbstractController {
	
	@RequestMapping(value = "/band", method = RequestMethod.GET)
	public ModelAndView newBand(HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "New Band";
		modelAndView = new ModelAndView("attraction/band/AddBand");
		modelAndView.addObject("pageName", pageName);
		return modelAndView;
	}
	
	@RequestMapping(value = "/band/{id}", method = RequestMethod.GET)
	public ModelAndView editBand(@PathVariable long id, HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "New Band";
		modelAndView = new ModelAndView("attraction/band/AddBand");
		modelAndView.addObject("pageName", pageName);
		modelAndView.addObject("band", BandManager.dao().get(id));
		return modelAndView;
	}
	
	@RequestMapping(value = "/band/remove/{id}", method = RequestMethod.GET)
	public String removeBand(@PathVariable long id, HttpSession session) {
		Band band = BandManager.dao().get(id);
		BandManager.dao().remove(band);
		return "redirect:/band/list";
	} 
	
	@RequestMapping(value = "/band/list", method = RequestMethod.GET)
	public ModelAndView listBand(HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "List Band";
		modelAndView = new ModelAndView("attraction/band/ListBand");
		modelAndView.addObject("pageName", pageName);
		modelAndView.addObject("bands", BandManager.dao().getAll());
		return modelAndView;
	}
	
	@RequestMapping(value = "/band", method = RequestMethod.POST)
	public String addBand(Band band, HttpServletRequest request, HttpSession session) {
		band.setImageList(getImages(request));
		for (Image img : band.getImageList()) {
			img.setBand(band);
		}
		BandManager.dao().add(band);
		return "redirect:band/list";
	}
	
	@RequestMapping(value = "band.json", method = RequestMethod.GET)
	public @ResponseBody
	List<EntityWrapper> getBandJson(@RequestParam String term, HttpSession session) {
		List<EntityWrapper> entityWrappers = new ArrayList<EntityWrapper>();
		entityWrappers = BandManager.findAllEntityWrappers(term);
		return entityWrappers;
	}
}
