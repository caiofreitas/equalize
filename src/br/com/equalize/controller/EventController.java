package br.com.equalize.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.equalize.manager.EventManager;
import br.com.equalize.model.Event;
import br.com.equalize.model.Image;
import br.com.equalize.model.wrapper.EntityWrapper;

@Controller
public class EventController extends AbstractController {
	
	@RequestMapping(value = "/event", method = RequestMethod.GET)
	public ModelAndView newEvent(HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "New Event";
		modelAndView = new ModelAndView("attraction/event/AddEvent");
		modelAndView.addObject("pageName", pageName);
		return modelAndView;
	}
	
	@RequestMapping(value = "/event/{id}", method = RequestMethod.GET)
	public ModelAndView editEvent(@PathVariable long id, HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "New Event";
		modelAndView = new ModelAndView("attraction/event/AddEvent");
		modelAndView.addObject("pageName", pageName);
		return modelAndView;
	}
	
	@RequestMapping(value = "/event/remove/{id}", method = RequestMethod.GET)
	public String removeEvent(@PathVariable long id, HttpSession session) {
		Event event = EventManager.dao().get(id);
		EventManager.dao().remove(event);
		return "redirect:/event/list";
	}
	
	@RequestMapping(value = "/event/list", method = RequestMethod.GET)
	public ModelAndView listEvent(HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "List Event";
		modelAndView = new ModelAndView("attraction/event/ListEvent");
		modelAndView.addObject("pageName", pageName);
		modelAndView.addObject("events", EventManager.dao().getAll());
		return modelAndView;
	}
	
	@RequestMapping(value = "/event", method = RequestMethod.POST)
	public String addEvent(Event event, HttpServletRequest request, HttpSession session) {
		event.setImageList(getImages(request));
		for (Image img : event.getImageList()) {
			img.setEvent(event);
		}
		EventManager.dao().add(event);
		return "redirect:event/list";
	}
	
	@RequestMapping(value = "event.json", method = RequestMethod.GET)
	public @ResponseBody
	List<EntityWrapper> getEnvetJson(@RequestParam String term, HttpSession session) {
		List<EntityWrapper> entityWrappers = new ArrayList<EntityWrapper>();
		return entityWrappers;
	}
}
