package br.com.equalize.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.equalize.manager.MovieManger;
import br.com.equalize.model.Image;
import br.com.equalize.model.Movie;
import br.com.equalize.model.wrapper.EntityWrapper;

@Controller
public class MovieController extends AbstractController {
	
	@RequestMapping(value = "/movie", method = RequestMethod.GET)
	public ModelAndView newMovie(HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "New Movie";
		modelAndView = new ModelAndView("attraction/movie/AddMovie");
		modelAndView.addObject("pageName", pageName);
		return modelAndView;
	}
	
	@RequestMapping(value = "/movie/{id}", method = RequestMethod.GET)
	public ModelAndView editMovie(@PathVariable("id") long id, HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "New Movie";
		modelAndView = new ModelAndView("attraction/movie/AddMovie");
		modelAndView.addObject("pageName", pageName);
		modelAndView.addObject("movie", MovieManger.dao().get(id));
		return modelAndView;
	}
	
	@RequestMapping(value = "/movie/remove/{id}", method = RequestMethod.GET)
	public String removeMovie(@PathVariable("id") long id, HttpSession session) {
		Movie movie = MovieManger.dao().get(id);
		MovieManger.dao().remove(movie);
		return "redirect:/movie/list";
	}
	
	@RequestMapping(value = "/movie/list", method = RequestMethod.GET)
	public ModelAndView listMovie(HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "List Movie";
		modelAndView = new ModelAndView("attraction/movie/ListMovie");
		modelAndView.addObject("pageName", pageName);
		modelAndView.addObject("movies", MovieManger.dao().getAll());
		return modelAndView;
	}
	
	@RequestMapping(value = "/movie", method = RequestMethod.POST)
	public String addMovie(Movie movie, HttpServletRequest request, HttpSession session) {
		movie.setImageList(getImages(request));
		for (Image img : movie.getImageList()) {
			img.setMovie(movie);
		}
		MovieManger.dao().add(movie);
		return "redirect:movie/list";
	}
	
	@RequestMapping(value = "movie.json", method = RequestMethod.GET)
	public @ResponseBody
	List<EntityWrapper> loadStates(@RequestParam String term, HttpSession session) {
		List<EntityWrapper> entityWrappers = new ArrayList<EntityWrapper>();
		entityWrappers = MovieManger.findAllEntityWrappers(term);
		return entityWrappers;
	}
}
