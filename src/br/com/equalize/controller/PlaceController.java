package br.com.equalize.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.equalize.manager.CityManager;
import br.com.equalize.manager.PlaceManager;
import br.com.equalize.model.Address;
import br.com.equalize.model.Image;
import br.com.equalize.model.Phone;
import br.com.equalize.model.Place;
import br.com.equalize.model.wrapper.EntityWrapper;

@Controller
public class PlaceController extends AbstractController {
	
	@RequestMapping(value = "/place", method = RequestMethod.GET)
	public ModelAndView newPlace(HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "New Place";
		modelAndView = new ModelAndView("place/AddPlace");
		modelAndView.addObject("pageName", pageName);
		return modelAndView;
	}
	
	@RequestMapping(value = "/place/{id}", method = RequestMethod.GET)
	public ModelAndView editPlace(@PathVariable long id, HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "New Place";
		Place place = PlaceManager.dao().get(id);
		modelAndView = new ModelAndView("place/AddPlace");
		modelAndView.addObject("pageName", pageName);
		modelAndView.addObject("place", place);
		return modelAndView;
	}
	
	@RequestMapping(value = "/place/remove/{id}", method = RequestMethod.GET)
	public String removePlace(@PathVariable long id, HttpSession session) {
		Place place = PlaceManager.dao().get(id);
		PlaceManager.dao().remove(place);
		return "redirect:/place/list";
	}
	
	@RequestMapping(value = "/place/list", method = RequestMethod.GET)
	public ModelAndView listPlace(HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "List Place";
		modelAndView = new ModelAndView("place/ListPlace");
		modelAndView.addObject("pageName", pageName);
		modelAndView.addObject("places", PlaceManager.dao().getAll());
		return modelAndView;
	}
	
	@RequestMapping(value = "/place", method = RequestMethod.POST)
	public String addPlace(HttpServletRequest request, HttpSession session) {
		Place place = new Place();
		place.setName(request.getParameter("name"));
		place.setDescription(request.getParameter("description"));
		place.setAddress(getAddress(request));
		place.setImageList(getImages(request));
		for (Image img : place.getImageList()) {
			img.setPlace(place);
		}
		Phone phone = new Phone();
		phone.setPlace(place);
		phone.setNumber(request.getParameter("phone"));
		List<Phone> phoneList = new ArrayList<Phone>();
		phoneList.add(phone);
		place.setPhoneList(phoneList);
		PlaceManager.dao().add(place);
		return "redirect:/place/list";
	}
	
	private Address getAddress(HttpServletRequest request) {
		Address address = new Address();
		address.setCep(request.getParameter("cep"));
		address.setComplement(request.getParameter("complement"));
		address.setNeighborhood(request.getParameter("neighborhood"));
		address.setStreet(request.getParameter("street"));
		address.setNumber(request.getParameter("number"));
		long id = Long.parseLong(request.getParameter("city"));
		address.setCity(CityManager.dao().get(id));
		return address;
	}
	
	@RequestMapping(value = "place.json", method = RequestMethod.GET)
	public @ResponseBody
	List<EntityWrapper> getPlaceJson(@RequestParam String term, HttpSession session) {
		List<EntityWrapper> entityWrappers = new ArrayList<EntityWrapper>();
		entityWrappers = PlaceManager.findAllEntityWrappers(term);
		return entityWrappers;
	}
}
