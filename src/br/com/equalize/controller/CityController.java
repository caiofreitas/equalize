package br.com.equalize.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.equalize.manager.CityManager;
import br.com.equalize.model.wrapper.CityWrapper;

@Controller
public class CityController extends AbstractController {
	
	@RequestMapping(value = "city.json", method = RequestMethod.GET)
	public @ResponseBody
	List<CityWrapper> getCity(@RequestParam String term, @RequestParam long stateId, HttpSession session) {
		List<CityWrapper> cityWrappers = new ArrayList<CityWrapper>();
		cityWrappers = CityManager.findAllEntityWrappers(term, stateId);
		return cityWrappers;
	}
}
