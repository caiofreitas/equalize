package br.com.equalize.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.equalize.manager.StateManager;
import br.com.equalize.model.wrapper.StateWrapper;

@Controller
public class StateController extends AbstractController {
	
	@RequestMapping(value = "state.json", method = RequestMethod.GET)
	public @ResponseBody
	List<StateWrapper> getStates(@RequestParam String term, HttpSession session) {
		List<StateWrapper> stateWrappers = StateManager.findAllEntityWrappers(term);
		return stateWrappers;
	}
}
