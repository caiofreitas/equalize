package br.com.equalize.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.equalize.manager.UserManager;
import br.com.equalize.model.User;

@Controller
public class UserController extends AbstractController {
	
	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public ModelAndView newUser(HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "New User";
		modelAndView = new ModelAndView("user/AddUser");
		modelAndView.addObject("pageName", pageName);
		return modelAndView;
	}
	
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public ModelAndView editUser(@PathVariable long id, HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "Edit User";
		modelAndView = new ModelAndView("user/AddUser");
		modelAndView.addObject("pageName", pageName);
		return modelAndView;
	}
	
	@RequestMapping(value = "/user/remove/{id}", method = RequestMethod.GET)
	public String removeUser(@PathVariable long id, HttpSession session) {
		return "redirect:/user/list";
	}
	
	@RequestMapping(value = "/user/list", method = RequestMethod.GET)
	public ModelAndView listUser(HttpSession session) {
		ModelAndView modelAndView;
		String pageName = "List User";
		modelAndView = new ModelAndView("user/ListUser");
		modelAndView.addObject("pagaName", pageName);
		modelAndView.addObject("users", UserManager.dao().getAll());
		return modelAndView;
	}
	
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public String addUser(User user, HttpSession session) {
		UserManager.dao().add(user);
		return "redirect:user/list";
	}
}
