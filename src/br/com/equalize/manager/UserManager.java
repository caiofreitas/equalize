package br.com.equalize.manager;

import br.com.equalize.model.User;

import com.im.imjutil.dao.DAO;
import com.im.imjutil.dao.jpa.JPADAO;
import com.im.imjutil.exception.ValidationException;
import com.im.imjutil.util.Filter;
import com.im.imjutil.validation.Validator;

public class UserManager {
	
	private UserManager() {
		super();
	}

	public static DAO<User> dao(){
		return new JPADAO<User>(User.class);
	}
	
	public static User findByNick(String nickName) {
		Filter filter = new Filter();
		filter.add("!%nickName!%", nickName);
		User user = dao().find(filter);
		return user;
	}
	
	public static User login(String login, String pass) {
		Filter filter = new Filter();
		filter.add("!%email!%", login);
		filter.add("!%password!%", pass);
		
		User user = dao().find(filter);
		return user;
	}
	
	public static boolean loginIsAvailable(String email) {
		if (!Validator.isValid(email))
			throw new ValidationException("Parametro invalido: email");
		
		Filter filter = new Filter();
		filter.add("!%email!%", email);
		User user = dao().find(filter);
		
		return (user == null);
	}
}
