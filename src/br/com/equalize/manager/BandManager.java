package br.com.equalize.manager;

import java.util.List;

import br.com.equalize.model.Band;
import br.com.equalize.model.wrapper.EntityWrapper;

import com.im.imjutil.dao.DAO;
import com.im.imjutil.dao.jpa.JPADAO;
import com.im.imjutil.query.Query;
import com.im.imjutil.query.QueryAdapter;
import com.im.imjutil.util.Filter;


public class BandManager {
	
	private BandManager(){
		super();
	}

	public static DAO<Band> dao(){
		return new JPADAO<Band>(Band.class);
	}
	
	public static List<EntityWrapper> findAllEntityWrappers(String term) {
		String sql = "select new br.com.equalize.model.wrapper.EntityWrapper(e.id, e.name) from Band e where e.name like :name";
		term = "%" + term + "%";
		Filter filter = new Filter();
		filter.add("name", term);
		Query query = new QueryAdapter(sql);
		List<EntityWrapper> result = dao().executeAll(query, filter);
		return result;
	}
}
