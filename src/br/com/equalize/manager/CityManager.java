package br.com.equalize.manager;

import java.util.List;

import br.com.equalize.model.City;
import br.com.equalize.model.wrapper.CityWrapper;

import com.im.imjutil.dao.DAO;
import com.im.imjutil.dao.jpa.JPADAO;
import com.im.imjutil.query.Query;
import com.im.imjutil.query.QueryAdapter;
import com.im.imjutil.util.Filter;

public class CityManager {

	public static DAO<City> dao(){
		return new JPADAO<City>(City.class);
	}
	
	public static City getByName(String name) {
		Filter filter = new Filter();
		filter.add("name", name);
		return dao().find(filter);
	}
	
	public static List<CityWrapper> findAllEntityWrappers(String term, long stateId) {
		String sql = "select new br.com.equalize.model.wrapper.CityWrapper(c.id, c.name, c.state.id) from City c where UPPER(c.name) like UPPER(:term) and c.state.id = :stateId";
		Filter filter = new Filter();
		filter.add("term", term);
		filter.add("stateId", stateId);
		Query query = new QueryAdapter(sql);
		List<CityWrapper> result = dao().executeAll(query, filter);
		return result;
	}
}
