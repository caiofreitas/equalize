package br.com.equalize.manager;

import br.com.equalize.model.Image;

import com.im.imjutil.dao.DAO;
import com.im.imjutil.dao.jpa.JPADAO;

public class ImageManager {
	
	private ImageManager() {
		super();
	}
	
	public static DAO<Image> dao(){
		return new JPADAO<Image>(Image.class);
	}
}
