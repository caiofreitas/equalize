package br.com.equalize.manager;

import java.util.List;

import br.com.equalize.model.State;
import br.com.equalize.model.wrapper.StateWrapper;

import com.im.imjutil.dao.DAO;
import com.im.imjutil.dao.jpa.JPADAO;
import com.im.imjutil.query.Query;
import com.im.imjutil.query.QueryAdapter;
import com.im.imjutil.util.Filter;

public class StateManager {

	private StateManager() {
		super();
	}
	
	public static DAO<State> dao(){
		return new JPADAO<State>(State.class);
	}
	
	public static List<StateWrapper> findAllEntityWrappers(String term) {
		String sql = "select new  br.com.equalize.model.wrapper.StateWrapper(s.id, s.name, s.acronym) from State s where UPPER(s.name) like UPPER(:term) OR UPPER(s.acronym) like UPPER(:term)";
		Filter filter = new Filter();
		filter.add("term", term);
		Query query = new QueryAdapter(sql);
		List<StateWrapper> result = dao().executeAll(query, filter);
		return result;
	}
}
