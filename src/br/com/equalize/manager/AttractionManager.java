package br.com.equalize.manager;

import br.com.equalize.model.Attraction;

import com.im.imjutil.dao.DAO;
import com.im.imjutil.dao.jpa.JPADAO;

public class AttractionManager {
	
	private AttractionManager() {
	}

	public static DAO<Attraction> dao(){
		return new JPADAO<Attraction>(Attraction.class);
	}
}
