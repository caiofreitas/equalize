package br.com.equalize.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class SecurityInterceptor extends HandlerInterceptorAdapter {

	private List<String> bypass;
	
	public boolean isBypass(String url) {
		for (String bp : bypass) {
			if ( url.startsWith(bp) )
				return true;
		}
		return false;
	}
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		String url = request.getRequestURI().replace(request.getContextPath(), "");
		
		if (request.getSession().getAttribute("user") == null && !isBypass(url)) {
			response.sendRedirect(request.getContextPath() + "/login?url=" + url);
			return false;
		}
		return true;
	}

	public List<String> getBypass() {
		return bypass;
	}

	public void setBypass(List<String> bypass) {
		this.bypass = bypass;
	}

}
