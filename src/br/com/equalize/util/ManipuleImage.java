package br.com.equalize.util;

import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.renderable.ParameterBlock;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.media.jai.Interpolation;
import javax.media.jai.InterpolationBilinear;
import javax.media.jai.JAI;
import javax.media.jai.OpImage;
import javax.media.jai.PlanarImage;
import javax.media.jai.RenderedOp;
import javax.media.jai.operator.ScaleDescriptor;

import com.im.imjutil.util.Pair;
import com.sun.media.jai.codec.SeekableStream;

/**
 * 
 * @author Caio Freitas
 *
 */
public class ManipuleImage {
	
	private ManipuleImage() {
		super();
	}
	
	/**
	 * C R O P
	 * 
	 * recorta uma imagem dado um ponto inicial e uma largura e altura
	 * 
	 * @param pImageData byte[] do aquivo da imagen a ser recordada
	 * @param topLeft_X coordenada x do ponto de origem do recorte
	 * @param topLeft_Y coordenada y do ponto de origem do recorte
	 * @param width largura do corte apartir do ponto (x,y) para direita
	 * @param height altura do corte apartir do ponto (x,y) para baixo
	 * @return byte[] da imagem recortada
	 */
	public static void cropImageAsJPG(File imageFile, float topLeft_X, float topLeft_Y, float width, float height)
			throws IOException {
	
		FileInputStream in = new FileInputStream(imageFile);
		
		// lendo a imagem original no input stream
		SeekableStream seekableStream = SeekableStream.wrapInputStream(in, true);
		RenderedOp originalImage = JAI.create("stream", seekableStream);
		((OpImage) originalImage.getRendering()).setTileCache(null);
		
		// setando parametros para o crop
		ParameterBlock paramBlock = new ParameterBlock();
		paramBlock.addSource(originalImage);
		
		paramBlock.add(topLeft_X);
		paramBlock.add(topLeft_Y);
		paramBlock.add(width);
		paramBlock.add(height);
		
		// para melhorar a qualidade da imagem
		RenderingHints qualityHints = new RenderingHints(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		
		// Crop da imagem
		RenderedOp cropedImage = JAI.create("crop", paramBlock, qualityHints);
		
		// Operação de escala que não altera o tamanho da image
		// para garantir que o ponto de origem (x,y) correto
		paramBlock = new ParameterBlock();
		paramBlock.addSource(cropedImage);
		paramBlock.add(1.0F);
		paramBlock.add(1.0F);
		paramBlock.add(0.0F);
		paramBlock.add(0.0F);
		cropedImage = JAI.create("scale", paramBlock, qualityHints);
		
		// por fim, gravar a imagem recém-redimensionar para
		// um fluxo de saída, em uma codificação específica
		ByteArrayOutputStream encoderOutputStream = new ByteArrayOutputStream();
		RenderedOp renderedOp = JAI.create("encode", cropedImage, encoderOutputStream, "JPEG", null);
		
		writeJPEG(renderedOp.getAsBufferedImage(), imageFile.getAbsolutePath());
	}
	
	
	public static Pair<Integer, Integer> getImageDimensions(File imageFile) {
		PlanarImage image = JAI.create("fileload", imageFile.getAbsolutePath());
		Pair<Integer, Integer> imageDimensions = new Pair<Integer, Integer>();
		imageDimensions.setFirst( image.getWidth());
		imageDimensions.setLast(image.getHeight());
		return imageDimensions;
	}
	
	/**
	 * R E S I Z E
	 * 
	 * redimensiona a imagem dada uma nova largura menor que a original e
	 * respeitando seu aspecto
	 * 
	 * @param imagePath path da imagem da imagem a ser redimensionada
	 * @param MaxWidth nova largura da image
	 * @throws IOException
	 */
	public static void resizeImageAsJPG(File imageFile, int maxWidth) throws IOException {
		PlanarImage image = JAI.create("fileload", imageFile.getAbsolutePath());
		float xScale = 1.0f;
		if (image.getWidth() > maxWidth) {
			xScale = (float) maxWidth / image.getWidth();
		}
		float yScale = 0f;
		yScale = xScale;
		RenderedOp renderedOp = ScaleDescriptor.create(image, new Float(xScale), new Float(yScale), new Float(0.0f),
				new Float(0.0f), Interpolation.getInstance(Interpolation.INTERP_BICUBIC), null);
		writeJPEG(renderedOp.getAsBufferedImage(), imageFile.getAbsolutePath());
	}
	
	@SuppressWarnings("cast")
	private static void writeJPEG(BufferedImage input, String name) throws IOException {
		Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName("JPG");
		if (iter.hasNext()) {
			ImageWriter writer = (ImageWriter) iter.next();
			ImageWriteParam iwp = writer.getDefaultWriteParam();
			iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			iwp.setCompressionQuality(0.95f);
			File outFile = new File(name);
			FileImageOutputStream output = new FileImageOutputStream(outFile);
			writer.setOutput(output);
			IIOImage image = new IIOImage(input, null, null);
			writer.write(null, image, iwp);
			output.close();
		}
	}
	
	public static void rotateImage(String imagePath, int degrees) throws IOException {
		
		// carrega a imagem indicada pelo endereço caminhoImagemRead the image
		PlanarImage image = JAI.create("fileload", imagePath);
		
		float angle = (float)Math.toRadians(degrees);
		float centerX = image.getWidth()/2f;
		float centerY = image.getHeight()/2f;
		ParameterBlock pb = new ParameterBlock();
		pb.addSource(image);
		pb.add(centerX);
		pb.add(centerY);
		pb.add(angle);
		pb.add(new InterpolationBilinear());
		PlanarImage rotatedImage = JAI.create("rotate", pb);
		
		ByteArrayOutputStream encoderOutputStream = new ByteArrayOutputStream();
		RenderedOp renderedOp = JAI.create("encode", rotatedImage, encoderOutputStream, "JPEG", null);
		
		writeJPEG(renderedOp.getAsBufferedImage(), imagePath);

	}
	
	
	public static void main(String[] args) {
		try {
			ManipuleImage.rotateImage("/home/caio/Pictures/Belladonna_2010.jpg", -45);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
}
